//
//  ThirdFormViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 19/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ThirdFormViewController: BaseViewController {

    @IBOutlet weak var vegTF: UITextField!
    @IBOutlet weak var allergyTF: UITextField!
    @IBOutlet weak var likesTF: UITextField!
    @IBOutlet weak var dislikeTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBActions
    @IBAction func onClickSkipButton(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "TabBarViewController", controllerName: self)
    }
    
    @IBAction func onClickNextButton(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
            let params = ["data":["foodspecificationList":[AppConstants.ServiceKeys.vegnonveg:vegTF.text!,AppConstants.ServiceKeys.allergies:allergyTF.text!,AppConstants.ServiceKeys.likes:likesTF.text!,AppConstants.ServiceKeys.dislikes:dislikeTF.text!]],"clientId":267] as [String : Any]
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formThreeServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formThreeServiceSuffix, notificationKey: AppConstants.SignupForm.formThreeServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
