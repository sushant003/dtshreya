//
//  UpdatedChatViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 17/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Firebase
import Photos
import Alamofire

class UpdatedChatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate {    
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var messageTV: UITextView!
    @IBOutlet var opponentName: UILabel!
    @IBOutlet var opponentImageView: CustomImage!
    @IBOutlet var scrollContentHeightConstraint: NSLayoutConstraint!
    @IBOutlet var chatTV: UITableView!
    @IBOutlet weak var scollView: UIScrollView!
    
    private let imageURLNotSetKey = "NOTSET"
    var scrolledToTop : Bool = false
    lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://dtshreya-766ef.appspot.com")
    private lazy var firebaseReference : DatabaseReference = DatabaseReference()
    private var updatedMessageRefHandle: DatabaseHandle?
    private var newMessageRefHandle: DatabaseHandle?
    var messageModel : [MessageModel] = [MessageModel]()
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        getCoversations()
        opponentName.text = (data as! DietitiansModel).name
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: (data as! DietitiansModel).url, imageView: opponentImageView, placeHolder: "")
        registerForKeyboardEvent()
        scollView.keyboardDismissMode = .onDrag
    }
    
    func getCoversations() {
        firebaseReference = Database.database().reference().child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))-\((data as! DietitiansModel).id)")
        newMessageRefHandle = firebaseReference.observe(.childAdded, with: {(snapshot) -> Void in
            let messageData = snapshot.value as! [String:Any]
            if let time = messageData["timeStamp"] as! String?, let text = messageData["message"] as! String?, text.count > 0,let _ = messageData["userModel"] {
                if (messageData["userModel"] as! [String:String])["name"] as String? ==  "Client\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))" {
                    let message = MessageModel(message: text, time: AppHelper.getInstance().getTimeFromEpoch(epochTime: Int64(time)!), type: 1)
                    self.messageModel.append(message)
                    self.insertCell()
                }
                else {
                    let message = MessageModel(message: text, time: AppHelper.getInstance().getTimeFromEpoch(epochTime: Int64(time)!), type: 2)
                    self.messageModel.append(message)
                    self.insertCell()
                }
            }
            else if let _ = messageData["file"],
                let time = messageData["timeStamp"] as! String? {
                let fileUrl = (messageData["file"] as! [String:String])["url_file"] as String?
                if (messageData["userModel"] as! [String:String])["name"] as String? == "Client\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))"{
                    let message = MessageModel(message: "", time: AppHelper.getInstance().getTimeFromEpoch(epochTime: Int64(time)!), image: fileUrl!, type: 3)
                    self.messageModel.append(message)
                    self.insertCell()
                }
                else {
                    let message = MessageModel(message: "", time: AppHelper.getInstance().getTimeFromEpoch(epochTime: Int64(time)!), image: fileUrl!, type: 4)
                    self.messageModel.append(message)
                    self.insertCell()
                }
            }
            else {
                print("Error! Could not decode message data")
            }
        })
    }
    
    func insertCell() {
        self.chatTV.insertRows(at: [IndexPath(row: (self.messageModel.count) - 1, section: 0)], with: UITableViewRowAnimation.automatic)
        let index: IndexPath = IndexPath(row: (self.messageModel.count) - 1, section: 0)
        DispatchQueue.main.async {
            if !self.scrolledToTop {
            self.chatTV.scrollToRow(at: index, at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: table view delegate and datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : UITableViewCell? = nil
        if messageModel[indexPath.row].type == 3 {
            cell = tableView.dequeueReusableCell(withIdentifier: "myImageAttachement", for: indexPath)
            DispatchQueue.main.async {
                (cell?.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "placeholder_attachement")
            }
            if messageModel[indexPath.row].attachedImage.hasPrefix("gs:") {
                let islandRef = Storage.storage().reference(forURL: messageModel[indexPath.row].attachedImage)
                islandRef.getData(maxSize: 8 * 1024 * 1024) { data, error in
                    if error != nil {
                    }
                    else {
                        let image = UIImage(data: data!)
                        (cell?.viewWithTag(1) as! UIImageView).image = image
                    }
                }
            }
            else {
            AppHelper.getInstance().loadImageIntoImageView(imageUrl: messageModel[indexPath.row].attachedImage, imageView: (cell?.viewWithTag(1) as! UIImageView), placeHolder: "placeholder_attachement")
            }
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 4 {
             cell = tableView.dequeueReusableCell(withIdentifier: "otherImageAttachement", for: indexPath)
            DispatchQueue.main.async {
                (cell?.viewWithTag(1) as! UIImageView).image = #imageLiteral(resourceName: "placeholder_attachement")
            }
            if messageModel[indexPath.row].attachedImage.hasPrefix("gs:") {
                let islandRef = Storage.storage().reference(forURL: messageModel[indexPath.row].attachedImage)
                islandRef.getData(maxSize: 8 * 1024 * 1024) { data, error in
                    if error != nil {
                    }
                    else {
                        let image = UIImage(data: data!)
                        (cell?.viewWithTag(1) as! UIImageView).image = image
                    }
                }
            }
            else {
                AppHelper.getInstance().loadImageIntoImageView(imageUrl: messageModel[indexPath.row].attachedImage, imageView: (cell?.viewWithTag(1) as! UIImageView), placeHolder: "placeholder_attachement")
            }
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "myChatCell", for: indexPath)
            (cell?.viewWithTag(1) as! UITextView).text = messageModel[indexPath.row].message
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        else if  messageModel[indexPath.row].type == 2 {
            cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath)
            (cell?.viewWithTag(1) as! UITextView).text = messageModel[indexPath.row].message
            (cell?.viewWithTag(2) as! UILabel).text = messageModel[indexPath.row].time
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if messageModel[indexPath.row].type == 1 || messageModel[indexPath.row].type == 2 {
            return 80
        }
        else {
            return 175
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAttachmentButton(_ sender: Any) {
        self.view.endEditing(true)
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onClickSendButton(_ sender: Any) {
        if messageTV.text == "" || messageTV.text == "Write your message" {
            return
        }
        let itemRef = Database.database().reference().child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))-\((data as! DietitiansModel).id)").childByAutoId()
        let messageDictionary = [
             "userModel": ["name": "Client\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))","photo_profile":"https://images-na.ssl-images-amazon.com/images/I/81wizIuFO1L._UX250_.jpg"],
            "message": messageTV.text!,
            "timeStamp":"\(AppHelper.getInstance().getEpochFromDate(date: Date()))"
            ] as [String : Any]
        let message = messageTV.text!
        self.view.endEditing(true)
        itemRef.setValue(messageDictionary)
        sendMessageNotification(message: message)
    }
    
    @IBAction func onClickOpenImage(_ sender: UIButton) {
        let indexPath : IndexPath = chatTV.indexPath(for: sender.superview?.superview?.superview as! UITableViewCell)!
        if messageModel[indexPath.row].type == 3 || messageModel[indexPath.row].type == 4 {
            AppHelper.getInstance().pushControllerWithData(id: "ImageViewController", controllerName: self, data: messageModel[indexPath.row].attachedImage)
        }
    }
    
    func sendMessageNotification(message: String) {
        let queue = DispatchQueue(label: "abc")
        queue.async {
            Alamofire.request(AppConstants.mainUrl.appending("chatnotification/"), method: HTTPMethod.post, parameters: ["to":(self.data as! DietitiansModel).id,"from":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),"message":message], encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseJSON {
                (response) in
                if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                }
                else {
                    print(response.error?.localizedDescription ?? "")
                }
            }
        }
    }
    
    func setImageURL(_ url: String) {
        let itemRef = Database.database().reference().child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))-\((data as! DietitiansModel).id)").childByAutoId()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-mm-dd"
        let messageItem = [
            "file":["name_file":(formatter.string(from: Date()) + "ios"),"type":"img","url_file":url],
            "userModel": ["name":  "Client\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))","photo_profile":"https://images-na.ssl-images-amazon.com/images/I/81wizIuFO1L._UX250_.jpg"],
            "message": "",
            "timeStamp":"\(AppHelper.getInstance().getEpochFromDate(date: Date()))"
            ] as [String : Any]
        itemRef.setValue(messageItem)
        sendMessageNotification(message: "\((data as! DietitiansModel).name) sent you a image")
    }
}

extension UpdatedChatViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if #available(iOS 11.0, *) {
            if let photoReferenceUrl = info[UIImagePickerControllerPHAsset] as? URL {
                let assets = PHAsset.fetchAssets(withALAssetURLs: [photoReferenceUrl], options: nil)
                let asset = assets.firstObject
                asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
                    let imageFileURL = contentEditingInput?.fullSizeImageURL
                    let path = "images/" + "\(photoReferenceUrl.lastPathComponent)"
                    self.storageRef.child(path).putFile(from: imageFileURL!, metadata: nil) { (metadata, error) in
                        if let error = error {
                            print("Error uploading photo: \(error.localizedDescription)")
                            return
                        }
                        self.setImageURL("\(metadata!.downloadURL()!)")
                    }
                })
            }
            else {
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData = UIImageJPEGRepresentation(image, 1.0)
                let imagePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                let metadata = StorageMetadata()
                metadata.contentType = "image/jpeg"
                let path = "images/" + "\(imagePath)"
                storageRef.child(path).putData(imageData!, metadata: metadata) { (metadata, error) in
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    self.setImageURL("\(metadata!.downloadURL()!)")
                }
            }
        } else {
            if let photoReferenceUrl = info[UIImagePickerControllerReferenceURL] as? URL {
                let assets = PHAsset.fetchAssets(withALAssetURLs: [photoReferenceUrl], options: nil)
                let asset = assets.firstObject
                asset?.requestContentEditingInput(with: nil, completionHandler: { (contentEditingInput, info) in
                    let imageFileURL = contentEditingInput?.fullSizeImageURL
                    let path = "images/" + "\(photoReferenceUrl.lastPathComponent)"
                    self.storageRef.child(path).putFile(from: imageFileURL!, metadata: nil) { (metadata, error) in
                        if let error = error {
                            print("Error uploading photo: \(error.localizedDescription)")
                            return
                        }
                        self.setImageURL("\(metadata!.downloadURL()!)")
                    }
                })
            }
            else {
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let imageData = UIImageJPEGRepresentation(image, 1.0)
                let imagePath = "\(Int(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                let metadata = StorageMetadata()
                let path = "images/" + "\(imagePath)"
                metadata.contentType = "image/jpeg"
                storageRef.child(path).putData(imageData!, metadata: metadata) { (metadata, error) in
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    self.setImageURL("\(metadata!.downloadURL()!)")
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: Textfield delegate
extension UpdatedChatViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.text = "Write your message"
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height >= 90 {
        }
        else {
            heightConstraint.constant = messageTV.contentSize.height
        }
        if messageModel.count != 0 {
            chatTV.scrollToRow(at: IndexPath(row: messageModel.count - 1, section: 0), at: .bottom, animated: false)
        }
    }
    
    func registerForKeyboardEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidOpen(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidClose(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unregisterKeyboardEvent() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil);NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardDidOpen(notification:Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            if scrollContentHeightConstraint.constant == 0 {
                let keyboardRectangle = keyboardFrame.cgRectValue
                scrollContentHeightConstraint.constant -= keyboardRectangle.height
                if messageModel.count != 0 {
                    chatTV.scrollToRow(at: IndexPath(row: messageModel.count - 1, section: 0), at: .bottom, animated: false)
                }
            }
        }
    }
    
    @objc func keyboardDidClose(notification:Notification) {
        if scrollContentHeightConstraint.constant != 0 {
            scrollContentHeightConstraint.constant = 0
        }
        heightConstraint.constant = 30
    }
}
