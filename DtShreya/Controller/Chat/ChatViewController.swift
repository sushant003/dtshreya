//
//  ChatViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 12/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class ChatViewController: BaseViewController {

    @IBOutlet weak var chatListTV: UITableView!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    @IBOutlet weak var serverStatusImage: UIImageView!
    var tableCellData : [DietitiansModel] = [DietitiansModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serverStatusImage.image = UIImage(named: "")
        getIds()
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.image = #imageLiteral(resourceName: "serverError")
        serverStatusLabel.text = text
    }
    
    
    //MARK: Webservice Method
    func getIds() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDietitianIDServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                for item in resultDict {
                    let  dietitian = DietitiansModel(name: item["name"] as! String, url: item["url"] as! String, id: item["id"] as! Int)
                    tableCellData.append(dietitian)
                }
                chatListTV.dataSource = self
                chatListTV.delegate = self
                chatListTV.reloadData()
            }
            else {
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
}

extension ChatViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableCellData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: tableCellData[indexPath.row].url, imageView: (cell.viewWithTag(1) as! UIImageView), placeHolder: "")
        let name : UILabel = cell.viewWithTag(2) as! UILabel
        let id : UILabel = cell.viewWithTag(3) as! UILabel
        name.text = tableCellData[indexPath.row].name
        id.text = "Id : \(tableCellData[indexPath.row].id)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AppHelper.getInstance().pushControllerWithData(id: "UpdatedChatViewController", controllerName: self, data: tableCellData[indexPath.row])
        
    }
}
