//
//  DiaryViewControllerViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 23/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class DiaryViewController: BaseViewController {

    @IBOutlet weak var diaryTV: UITableView!
    @IBOutlet weak var wakeUpTimeLabel: UILabel!
    @IBOutlet weak var totalWeightLabel: UILabel!
    @IBOutlet weak var greenTeaLabel: UILabel!
    @IBOutlet weak var sleepTimeLabel: UILabel!
    @IBOutlet weak var waterIntakeLabel: UILabel!
    @IBOutlet weak var poppedLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateCV: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var serverStatusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    
    var tableData : [MealModel] = [MealModel]()
    var hasDiary : Bool = false
    var selectedIndex : Int = 0
    var diary : DiaryModel = DiaryModel(sleeptime: "", wakeuptime: "", todayweight: "", greentea: "", waterintake: "", pooped: "")
    override func viewDidLoad() {
        super.viewDidLoad()
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        diaryTV.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        collectionViewFlowLayout.itemSize = CGSize(width: view.frame.width/7, height: 60)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-yyyy"
        dateLabel.text = formatter.string(from: Date())
        serverStatusImage.image = UIImage(named: "")
        getClientDiary()
    }
    
    override func viewDidLayoutSubviews() {
        for cell in diaryTV.visibleCells {
            (cell.viewWithTag(1) as! CustomView).createShadow()
        }
    }
    
    func setServerStatus(text : String) {
        serverStatusView.isHidden = false
        serverStatusImage.image = #imageLiteral(resourceName: "serverError")
        serverStatusLabel.text = text
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickCreateDiary(_ sender: Any) {
        let diaryDate : Date = Calendar.current.date(byAdding: .day, value: selectedIndex, to: Date())!
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        AppHelper.getInstance().pushControllerWithData(id: "CreateDiaryViewController", controllerName: self, data: ["date":formatter.string(from: diaryDate),"basicInfo":diary,"food":tableData,"hasData":hasDiary])
    }
    
    //Webservice Methods
    func getClientDiary() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            let diaryDate : Date = Calendar.current.date(byAdding: .day, value: selectedIndex, to: Date())!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            NotificationCenter.default.addObserver(self,selector: #selector(self.diaryCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diary_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: diaryDate)], url: AppConstants.Clients.get_diary_service_suffix, notificationKey: AppConstants.Clients.get_diary_service_notify, controllerName: self, requestType: "")
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Net_Error)
        }
    }
    
    @objc func diaryCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [String:Any])!
                prepareDiaryData(serviceData: resultDict)
                hasDiary = true
                diaryTV.dataSource = self
                diaryTV.reloadData()
            }
            else {
                hasDiary = false
                setServerStatus(text: message!)
            }
        }
        else {
            setServerStatus(text: AppConstants.ErrorMessags.Server_Error)
        }
    }
    
    func prepareDiaryData(serviceData: [String:Any]) {
        wakeUpTimeLabel.text = "Wake up time: \(serviceData[AppConstants.ServiceKeys.wakeuptime]!)"
        totalWeightLabel.text = "Todays weight: \(serviceData[AppConstants.ServiceKeys.todayWeight]!)"
        greenTeaLabel.text = "Green tea: \(serviceData[AppConstants.ServiceKeys.greenTea]!) cups"
        sleepTimeLabel.text = "Sleep time: \(serviceData[AppConstants.ServiceKeys.sleepTime]!)"
        waterIntakeLabel.text = "Water intake: \(serviceData[AppConstants.ServiceKeys.waterIntake]!) glass"
        poppedLabel.text = "Pooped: \(serviceData[AppConstants.ServiceKeys.pooped]!) times"
        diary = DiaryModel(sleeptime: "\(serviceData[AppConstants.ServiceKeys.sleepTime]!)", wakeuptime: "\(serviceData[AppConstants.ServiceKeys.wakeuptime]!)", todayweight: "\(serviceData[AppConstants.ServiceKeys.todayWeight]!)", greentea: "\(serviceData[AppConstants.ServiceKeys.greenTea]!)", waterintake: "\(serviceData[AppConstants.ServiceKeys.waterIntake]!)", pooped: "\(serviceData[AppConstants.ServiceKeys.pooped]!)")
        for (key,value) in serviceData["dietdata"] as! [String:String] {
            var meal : MealModel!
            switch key {
            case AppConstants.ServiceKeys.firstMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: [value], mealIndex: 0)
            case AppConstants.ServiceKeys.secondMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: [value], mealIndex: 1)
            case AppConstants.ServiceKeys.thirdMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: [value], mealIndex: 2)
            case AppConstants.ServiceKeys.fourthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: [value], mealIndex: 3)
            case AppConstants.ServiceKeys.fifthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: [value], mealIndex: 4)
            case AppConstants.ServiceKeys.sixthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: [value], mealIndex: 5)
            case AppConstants.ServiceKeys.seventhMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: [value], mealIndex: 6)
            case AppConstants.ServiceKeys.eighthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: [value], mealIndex: 7)
            case AppConstants.ServiceKeys.supplements:
                meal = MealModel(name: AppConstants.ServiceKeys.supplementsName, meal: [value], mealIndex: 8)
            default:
                break
            }
            if key != "foodnine" {
                tableData.append(meal!)
            }
        }
        tableData = tableData.sorted(by: { (first, second) -> Bool in
            first.index < second.index
        })
    }
}

extension DiaryViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell.viewWithTag(2) as! UILabel).text = tableData[indexPath.row].mealTimeName
        (cell.viewWithTag(3) as! UILabel).text = tableData[indexPath.row].meal[0]
        return cell
    }
}

//MARK: UICollectionview datasource and delegate
extension DiaryViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath)
        let dayName : UILabel = cell.viewWithTag(1) as! UILabel
        let dayNumber : UILabel = cell.viewWithTag(2) as! UILabel
        let cellDate : Date = Calendar.current.date(byAdding: .day, value: indexPath.row, to: Date())!
        let dayNameFormatter = DateFormatter()
        let dayNumberFormatter = DateFormatter()
        if selectedIndex == indexPath.item {
            dayNumber.backgroundColor = UIColor.white
            dayNumber.textColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        }
        else {
            dayNumber.backgroundColor = UIColor.clear
            dayNumber.textColor = UIColor.white
        }
        dayNumber.layer.masksToBounds = true
        dayNameFormatter.dateFormat = "EEE"
        dayNumberFormatter.dateFormat = "dd"
        dayName.text = dayNameFormatter.string(from: cellDate)
        dayNumber.text = dayNumberFormatter.string(from: cellDate)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        tableData.removeAll()
        collectionView.reloadData()
        getClientDiary()
    }
}
