//
//  SelectDietitianViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 13/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class SelectDietitianViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension SelectDietitianViewController : UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((data as! [String:Any])["data"] as! [DietitiansModel]).count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        AppHelper.getInstance().loadImageIntoImageView(imageUrl: ((data as! [String:Any])["data"] as! [DietitiansModel])[indexPath.row].url, imageView: (cell.viewWithTag(1) as! UIImageView), placeHolder: "")
        (cell.viewWithTag(2) as! UILabel).text = ((data as! [String:Any])["data"] as! [DietitiansModel])[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ((data as! [String:Any])["controller"] as! throwData).sendPopedData(data: ((data as! [String:Any])["data"] as! [DietitiansModel])[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
}
