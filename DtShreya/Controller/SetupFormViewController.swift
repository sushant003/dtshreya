//
//  SetupFormViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 26/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class SetupFormViewController: BaseViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var anniversaryTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var whatsappTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    @IBOutlet weak var occupationTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var referredByTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var feetTF: UITextField!
    @IBOutlet weak var incheTF: UITextField!
    var selectedWeightType = "kgs"
    var selectedGender = "Male"
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //MARK: Actions
    
    @IBAction func tappedWeightSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            selectedWeightType = "kgs"
        }
        else {
            selectedWeightType = "lbs"
        }
    }
    
    @IBAction func tappedGenderSegment(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            selectedWeightType = "Male"
        }
        else {
            selectedWeightType = "Female"
        }
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
            if (nameTF.text == "" || dobTF.text == "" || contactTF.text == "" || whatsappTF.text == "" || addressTF.text == "" || emailTF.text == "" || weightTF.text == "" || feetTF.text == "") {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please fill all the relevant data.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if (contactTF.text?.count)! > 16 || (contactTF.text?.count)! < 6 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter contact number between 6 to 16 digits", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if !AppHelper.getInstance().isValidEmail(testStr: emailTF.text!) {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Email_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }            
            let params = ["data":["detailList":[AppConstants.ServiceKeys.name:nameTF.text!,AppConstants.ServiceKeys.dob:dobTF.text!,AppConstants.ServiceKeys.anniversary:anniversaryTF.text!,AppConstants.ServiceKeys.contact:contactTF.text!,AppConstants.ServiceKeys.whatsapp:whatsappTF.text!,AppConstants.ServiceKeys.address:addressTF.text!,AppConstants.ServiceKeys.occupation:occupationTF.text!,AppConstants.ServiceKeys.email:emailTF.text!,AppConstants.ServiceKeys.referredBy:referredByTF.text!,AppConstants.ServiceKeys.weight:weightTF.text! + selectedWeightType,AppConstants.ServiceKeys.height:(feetTF.text! + (incheTF.text == "" ? "" : ("'" + incheTF.text!))),AppConstants.ServiceKeys.gender:selectedGender]],AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)]
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formOneServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formOneServiceSuffix, notificationKey: AppConstants.SignupForm.formOneServiceNotify, controllerName: self,requestType: "")
        }
        else {
           AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().pushView(identifier: "SecondFormViewController", controllerName: self)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
}
