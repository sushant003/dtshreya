//
//  CreateDiaryViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 13/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class CreateDiaryViewController: BaseViewController {

    @IBOutlet weak var wakeUpLabel: UILabel!
    @IBOutlet weak var sleepTimeLabel: UILabel!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var waterTF: UITextField!
    @IBOutlet weak var teaTF: UITextField!
    @IBOutlet weak var poopedTF: UITextField!
    @IBOutlet weak var morningTF: UITextField!
    @IBOutlet weak var breakfastTF: UITextField!
    @IBOutlet weak var midMorningTF: UITextField!
    @IBOutlet weak var lunchTF: UITextField!
    @IBOutlet weak var eveningTF: UITextField!
    @IBOutlet weak var lateEveningTF: UITextField!
    @IBOutlet weak var dinnerTF: UITextField!
    @IBOutlet weak var postDinnerTF: UITextField!
    @IBOutlet weak var supplementsTF: UITextField!
    
    var timePickerController : DatePickerViewController! = nil
    var selectedIndex : Int = 0
    var mealData : [MealModel] =  [MealModel]()
    var tableDict : [String] = ["Wake Up Time","Sleep Time","Today's Weight","Water Intake","Green Tea","No. of Times Pooped","Early Morning","Breakfast","Mid Morning","Lunch","Evening","Late Evening","Dinner","Post Dinner","Supplements"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timePickerController = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        populateView()
    }
    
    func populateView() {
        wakeUpLabel.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).wakeuptime
        sleepTimeLabel.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).sleeptime
        weightTF.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).todayweight
        waterTF.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).waterintake
        teaTF.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).greentea
        poopedTF.text = ((data as! [String:Any])["basicInfo"] as! DiaryModel).pooped
        if ((data as! [String:Any])["hasData"] as! Bool) {
            setMealData(meals: ((data as! [String:Any])["food"] as! [MealModel]))
        }
        else {
            getDateDiet()
        }
    }
    
    func setMealData(meals : [MealModel]) {
        for meal in meals {
            switch meal.mealTimeName {
            case AppConstants.ServiceKeys.firstMeal:
                morningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.secondMeal:
                breakfastTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.thirdMeal:
                midMorningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.fourthMeal:
                lunchTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.fifthMeal:
                eveningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.sixthMeal:
                lateEveningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.seventhMeal:
                dinnerTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.eighthMeal:
                postDinnerTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.supplements:
                supplementsTF.text = meal.meal.joined(separator: ",")
            default:
                break
            }
        }
    }
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onTapWakeUp(_ sender: Any) {
        self.view.endEditing(true)
        let dateFormatter = DateFormatter()
        selectedIndex = 1
        dateFormatter.dateFormat = "dd-MM-yyyy"
        timePickerController.datePickerDelegate = self
        timePickerController.data = ["pickerType":"time","date":dateFormatter.string(from: Date())]
        addChildViewController(timePickerController)
        view.addSubview(timePickerController.view)
        timePickerController.didMove(toParentViewController: self)
    }
    
    @IBAction func onTapsleepTime(_ sender: Any) {
        self.view.endEditing(true)
        let dateFormatter = DateFormatter()
        selectedIndex = 2
        dateFormatter.dateFormat = "dd-MM-yyyy"
        timePickerController.datePickerDelegate = self
        timePickerController.data = ["pickerType":"time","date":dateFormatter.string(from: Date())]
        addChildViewController(timePickerController)
        view.addSubview(timePickerController.view)
        timePickerController.didMove(toParentViewController: self)
    }
    
    func prepareDietData() -> [String:String] {
        return [AppConstants.ServiceKeys.firstMeal:morningTF.text!,AppConstants.ServiceKeys.secondMeal:breakfastTF.text!,AppConstants.ServiceKeys.thirdMeal:midMorningTF.text!,AppConstants.ServiceKeys.fourthMeal:lunchTF.text!,AppConstants.ServiceKeys.fifthMeal:eveningTF.text!,AppConstants.ServiceKeys.sixthMeal:lateEveningTF.text!,AppConstants.ServiceKeys.seventhMeal:dinnerTF.text!,AppConstants.ServiceKeys.eighthMeal:postDinnerTF.text!,AppConstants.ServiceKeys.supplements:supplementsTF.text!]
    }
    
    @IBAction func onClickSaveButton(_ sender: Any) {
        let params = [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),
                      AppConstants.ServiceKeys.dateOfDiet:(data as! [String:Any])["date"],
                      AppConstants.ServiceKeys.sleepTime:(sleepTimeLabel.text! == "" ? "00:00" : sleepTimeLabel.text!),
                      AppConstants.ServiceKeys.wakeuptime:(wakeUpLabel.text! == "" ? "00:00" : wakeUpLabel.text!),
                      AppConstants.ServiceKeys.todayWeight:weightTF.text!,
                      AppConstants.ServiceKeys.greenTea:teaTF.text!,
                      AppConstants.ServiceKeys.waterIntake:waterTF.text!,
                      AppConstants.ServiceKeys.pooped:poopedTF.text!,
                      AppConstants.ServiceKeys.dietData:prepareDietData()
        ]
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Appointments.create_diary_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.Appointments.create_diary_service_suffix, notificationKey: AppConstants.Appointments.create_diary_service_notify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                navigationController?.popViewController(animated: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func getDateDiet() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:(data as! [String:Any])["date"]!], url: AppConstants.Clients.get_diet_service_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any])
                prepareTableData(serviceData: resultDict)
                
            }
        }
    }
    
    func prepareTableData(serviceData : [String:Any]) {
        for (key,value) in serviceData  {
            var meal : MealModel!
            if key == AppConstants.ServiceKeys.notes {
                continue
            }
            switch key {
            case AppConstants.ServiceKeys.firstMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: value as! [String], mealIndex: 0)
            case AppConstants.ServiceKeys.secondMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: value as! [String], mealIndex: 1)
            case AppConstants.ServiceKeys.thirdMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: value as! [String], mealIndex: 2)
            case AppConstants.ServiceKeys.fourthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: value as! [String], mealIndex: 3)
            case AppConstants.ServiceKeys.fifthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: value as! [String], mealIndex: 4)
            case AppConstants.ServiceKeys.sixthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: value as! [String], mealIndex: 5)
            case AppConstants.ServiceKeys.seventhMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: value as! [String], mealIndex: 6)
            case AppConstants.ServiceKeys.eighthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: value as! [String], mealIndex: 7)
            case AppConstants.ServiceKeys.supplements:
                meal = MealModel(name: AppConstants.ServiceKeys.supplementsName, meal: value as! [String], mealIndex: 8)
            default:
                break
            }
            if key != "foodnine" {
                mealData.append(meal!)
            }
        }
        setMeal(meals: mealData)
    }
    
    func setMeal(meals : [MealModel]) {
        for meal in meals {
            switch meal.mealTimeName {
            case AppConstants.ServiceKeys.firstMealName:
                morningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.secondMealName:
                breakfastTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.thirdMealName:
                midMorningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.fourthMealName:
                lunchTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.fifthMealName:
                eveningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.sixthMealName:
                lateEveningTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.seventhMealName:
                dinnerTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.eightMealName:
                postDinnerTF.text = meal.meal.joined(separator: ",")
            case AppConstants.ServiceKeys.supplementsName:
                supplementsTF.text = meal.meal.joined(separator: ",")
            default:
                break
            }
        }
    }
}

//MARK: Datepicker Delegate

extension CreateDiaryViewController : DatePickerListener {
    func setDateFromPicker(date: Any) {
        timePickerController.view.removeFromSuperview()
        if selectedIndex == 1 {
            wakeUpLabel.text = (date as! String).components(separatedBy: " ")[1]
        }
        else {
            sleepTimeLabel.text = (date as! String).components(separatedBy: " ")[1]
        }
    }
    
    func dismissDatePicker() {
        timePickerController.view.removeFromSuperview()
    }
}
