//
//  ReminderViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class ReminderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var reminderData: [Reminder] = [Reminder(meal: MealType.breakfast, reminderTime: (6, 00)),
                                    Reminder(meal: MealType.lunch, reminderTime: (1, 10)),
                                    Reminder(meal: MealType.postLunch, reminderTime: (3, 10)),
                                    Reminder(meal: MealType.eveningSnack, reminderTime: (5, 10)),
                                    Reminder(meal: MealType.dinner, reminderTime: (8, 10)),
                                    Reminder(meal: MealType.postDinner, reminderTime: (11, 10))]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reminderData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reminderCell") as! ReminderTableViewCell
        cell.setCell(reminder: reminderData[indexPath.row])
        return cell
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
