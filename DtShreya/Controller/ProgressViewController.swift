//
//  ProgressViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 20/04/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import AVFoundation

class ProgressViewController: BaseViewController {
    
    @IBOutlet weak var rankCV: UICollectionView!
    @IBOutlet weak var badgeCV: UICollectionView!
    @IBOutlet weak var ringIcon: UIImageView!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var badgeCountLabel: UILabel!
    @IBOutlet weak var bellsCountLabel: UILabel!
    @IBOutlet weak var kgLostLabel: UILabel!
    var progressModel : ProgressModel = ProgressModel()
    @IBOutlet weak var rankCVFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var badgeCVFlowLayout: UICollectionViewFlowLayout!
    let imageArray : [String:[UIImage]] = ["rank":[#imageLiteral(resourceName: "rank1"),#imageLiteral(resourceName: "rank2"),#imageLiteral(resourceName: "rank3"),#imageLiteral(resourceName: "rank4")],"badge":[#imageLiteral(resourceName: "badge1"),#imageLiteral(resourceName: "badge2"),#imageLiteral(resourceName: "badge3"),#imageLiteral(resourceName: "badge4"),#imageLiteral(resourceName: "badge5")]]
    let badgeNameArray : [String] = ["Can do it","Enthusiastic","Disciplined","Going awesome","Dietitian's favorite"]
    var selectedBadgeIndex = 0
    var player: AVAudioPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        rankCVFlowLayout.itemSize = CGSize(width: self.view.frame.width/4 - 5, height: self.view.frame.width/4 - 5)
        badgeCVFlowLayout.itemSize = CGSize(width: self.view.frame.width/2, height: 120)
        getProgressData()
    }
    
    @IBAction func onTapRingBell(_ sender: Any) {
        AppHelper.getInstance().showAlert(alertTitle: "Confirm Bell", message: "Ringing the bell means you've lost 0.5 kgs. Do you wish to continue?", cancelButtonName: "No", otherButtonName: "Yes", controllerName: self, isAlertOnly: false)
    }
    
    
    //MARK: Webservice methods
    func getProgressData() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.progressCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_progress_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.get_progress_service_suffix, notificationKey: AppConstants.Clients.get_progress_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func progressCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [String:Any])!
                prepareProgressModel(resultDict: resultDict)
                
                rankCV.dataSource = self
                badgeCV.dataSource = self
                rankCV.reloadData()
                badgeCV.reloadData()
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func ringBell() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.ringCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.ring_bell_service_notify),object: nil)
            Webservice.getInstance().hitService(parameter: ["clientId":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Clients.ring_bell_service_suffix, notificationKey: AppConstants.Clients.ring_bell_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func ringCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                playSound()
                shake()
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "congrats", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 5
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: ringIcon.center.x - 10, y: ringIcon.center.y)
        animation.toValue = CGPoint(x: ringIcon.center.x + 10, y: ringIcon.center.y)
        ringIcon.layer.add(animation, forKey: "position")
    }
    
    func prepareProgressModel(resultDict : [String:Any]) {
        progressModel.level = Int("\(resultDict["level"]!)")
        progressModel.badge = Int("\(resultDict["badge"]!)")
        progressModel.bells = Int("\(resultDict["bells"]!)")
        progressModel.kgslost = Float("\(resultDict[AppConstants.ServiceKeys.kgsLost]!)")
        
        switch progressModel.level{
        case 1:
            levelLabel.text = "Newbie"
        case 2:
            levelLabel.text = "Senior"
        case 3:
            levelLabel.text = "Pro"
        case 4:
            levelLabel.text = "Super Pro"
        default:
            break
        }
        badgeCountLabel.text = "\(progressModel.badge!)"
        bellsCountLabel.text = "\(progressModel.bells!) Bells Rung"
        kgLostLabel.text = "\(progressModel.kgslost!) Kgs Lost"
    }
   
}

extension ProgressViewController : UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == rankCV {
            return 4
        }
        else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if collectionView == rankCV {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rank", for: indexPath)
            let rankImage : UIImageView = cell.viewWithTag(1) as! UIImageView
            let fadedView : UIView = cell.viewWithTag(2)!
            rankImage.image = imageArray["rank"]?[indexPath.item]
            if indexPath.row < progressModel.level {
                fadedView.isHidden = true
            }
            else {
                fadedView.isHidden = false
            }
        }
        else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "badge", for: indexPath)
            let badgeImage : UIImageView = cell.viewWithTag(1) as! UIImageView
            let badgeLabel : UILabel = cell.viewWithTag(2) as! UILabel
            let fadedView : UIView = cell.viewWithTag(3)!
            badgeImage.image = imageArray["badge"]?[indexPath.item]
            if indexPath.row < progressModel.badge {
                fadedView.isHidden = true
            }
            else {
                fadedView.isHidden = false
            }
            badgeLabel.text = badgeNameArray[indexPath.item]
        }
        return cell
    }
}

extension ProgressViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        ringBell()
    }
    
    func alertNegativeButtonPressed() {
    }
}
