//
//  SignUpViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 08/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func signUpButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        if usernameTextField.text?.count != 0 && passwordTextField.text?.count != 0 && emailTextField.text?.count != 0 {
            let params = ["username": usernameTextField.text!, "email": emailTextField.text!, "password": passwordTextField.text!]
            if usernameTextField.text == "" || emailTextField.text == "" || passwordTextField.text == "" || confirmPasswordTF.text == "" {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter all the details", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if !AppHelper.getInstance().isValidEmail(testStr: emailTextField.text!) {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Email_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            else if (passwordTextField.text?.count)! < 6 {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            else if passwordTextField.text != confirmPasswordTF.text {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Match_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            if !AppHelper.getInstance().isNetworkAvailable() {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                return
            }
            DispatchQueue.main.async {
                AppHelper.getInstance().showProgressIndicator(view: self.view)
            }
            DataService.ds.postRequest(urlExt: "signup/", params: params) { (data, error) in
                if let data = data {
                    let message  = data[AppConstants.ServiceKeys.message] as? String
                    if let status = data[AppConstants.ServiceKeys.status] as? Bool {
                        if status == true {
                            self.saveLopinData(dict: data)
                            DispatchQueue.main.async {
                                self.loginToFirebase()
                            }
                        }
                    }
                    else {
                        DispatchQueue.main.async {
                            AppHelper.getInstance().hideProgressIndicator(view: self.view)
                            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                        }
                    }
                }
                else {
                    DispatchQueue.main.async {
                        AppHelper.getInstance().hideProgressIndicator(view: self.view)
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    }
                }
            }
        }
    }
    
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func saveLopinData(dict : [String:Any]) {
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isLoggedIn)
        AppHelper.getInstance().setValueForUserDefault(value: usernameTextField.text!, key: AppConstants.userDefaultConstants.userName)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.userId]!, key:  AppConstants.ServiceKeys.userId)
        AppHelper.getInstance().setValueForUserDefault(value: "N", key: AppConstants.ServiceKeys.dieterStatus)
        AppHelper.getInstance().setValueForUserDefault(value: emailTextField.text!, key: AppConstants.ServiceKeys.email)
    }
    
    //MARK: Firebase methods
    func loginToFirebase() {
        Auth.auth().signIn(withEmail: self.emailTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            if error == nil {
                AppHelper.getInstance().pushView(identifier: "SetupFormViewController", controllerName: self)
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                    if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.user_device_token) as? String)  != nil {
                    }
                }
            }
            else {
                self.createFirebaseAccount()
            }
        }
    }
    
    func createFirebaseAccount() {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                AppHelper.getInstance().pushView(identifier: "SetupFormViewController", controllerName: self)
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                    if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.user_device_token) as? String)  != nil {
                        
                    }
                }
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (error?.localizedDescription)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
    }
}
