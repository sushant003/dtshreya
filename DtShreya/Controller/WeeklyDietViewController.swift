//
//  WeeklyDietViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 12/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class WeeklyDietViewController: BaseViewController {

    @IBOutlet weak var dietTableView: UITableView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateCV: UICollectionView!
    @IBOutlet weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var statusView: UIView!
    var recipeName : String = ""
    var selectedIndex : Int = 0
    var mealData : [String:[MealModel]] =  [String:[MealModel]]()
    var sortedDateArray : [String]? = nil
    var notes : [String:[String]] = [String:[String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusView.isHidden = true
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        dietTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        collectionViewFlowLayout.itemSize = CGSize(width: view.frame.width/7, height: 60)
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-yyyy"
        dateLabel.text = formatter.string(from: Date())
        getDietOfSevenDays()
    }
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickNotes(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "DietNotesViewController", controllerName: self, data: (notes[sortedDateArray![selectedIndex]] == nil ? [] : notes[sortedDateArray![selectedIndex]]!))
    }
    
    @IBAction func onClickShopping(_ sender: Any) {
        getShoppingList()
    }
    
    func getRecepie(foodName : String) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getRecepieCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.name:foodName], url: AppConstants.Clients.get_recepie_service_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getRecepieCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: "Recipe", message: (serviceData.userInfo?["recipe"] as? String) == nil ? "N/A" : (serviceData.userInfo?["recipe"] as? String)!, cancelButtonName: "OK", otherButtonName: "Copy", controllerName: self, isAlertOnly: false)
                recipeName = (serviceData.userInfo?["recipe"] as? String) == nil ? "N/A" : (serviceData.userInfo?["recipe"] as? String)!
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func getShoppingList() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getShoppingCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_seven_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Calendar.current.date(byAdding: .day, value: selectedIndex, to: Date())!)], url: AppConstants.Clients.shopping_list_service_suffix, notificationKey: AppConstants.Clients.get_seven_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getShoppingCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultData: [String] = (serviceData.userInfo?["list"] as! [String])
                AppHelper.getInstance().pushControllerWithData(id: "ShoppingViewController", controllerName: self, data: resultData)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func getDietOfSevenDays() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_seven_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"//formatter.string(from: Date())
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.get_seven_diet_service_suffix, notificationKey: AppConstants.Clients.get_seven_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any])
                prepareTableData(serviceData: resultDict)
                dietTableView.dataSource = self
                dietTableView.delegate = self
                dietTableView.reloadData()
            }
            else {
                statusView.isHidden = false
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func prepareTableData(serviceData : [String:Any]) {
        for (keys,values) in serviceData {
            var meals : [MealModel] = [MealModel]()
            if values as? [String:Any] != nil {
                for (key,value) in values as! [String:Any] {
                    var meal : MealModel!
                    if key == AppConstants.ServiceKeys.notes {
                        notes[keys] = value as? [String]
                        continue
                    }
                    switch key {
                    case AppConstants.ServiceKeys.firstMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: value as! [String], mealIndex: 0)
                    case AppConstants.ServiceKeys.secondMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: value as! [String], mealIndex: 1)
                    case AppConstants.ServiceKeys.thirdMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: value as! [String], mealIndex: 2)
                    case AppConstants.ServiceKeys.fourthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: value as! [String], mealIndex: 3)
                    case AppConstants.ServiceKeys.fifthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: value as! [String], mealIndex: 4)
                    case AppConstants.ServiceKeys.sixthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: value as! [String], mealIndex: 5)
                    case AppConstants.ServiceKeys.seventhMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: value as! [String], mealIndex: 6)
                    case AppConstants.ServiceKeys.eighthMeal:
                        meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: value as! [String], mealIndex: 7)
                    case AppConstants.ServiceKeys.supplements:
                            meal = MealModel(name: AppConstants.ServiceKeys.supplementsName, meal: value as! [String], mealIndex: 8)
                    default:
                        break
                    }
                    if key != "foodnine" {
                        meals.append(meal)
                    }
                }
            }
            meals = meals.sorted(by: { (first, second) -> Bool in
                first.index < second.index
            })
            mealData[keys] = meals
        }
        sortedDateArray = Array(mealData.keys).sorted()
    }
}

extension WeeklyDietViewController : alertPositiveButtonDelegate {
    func alertNegativeButtonPressed() {
    }
    
    func alertPositiveButtonPressed() {
        UIPasteboard.general.string = recipeName
    }
}

//MARK: Tableview datasource and delegate
extension WeeklyDietViewController : UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (mealData[sortedDateArray![selectedIndex]]?.count)!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mealData[sortedDateArray![selectedIndex]]![section].collapsed ? 0 : mealData[sortedDateArray![selectedIndex]]![section].meal.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.delegate = self
        headerView.seactionIndex = section
        headerView.isCollapsed = mealData[sortedDateArray![selectedIndex]]![section].collapsed
        headerView.headerLabel.text = mealData[sortedDateArray![selectedIndex]]![section].mealTimeName
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row]
         cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getRecepie(foodName: mealData[sortedDateArray![selectedIndex]]![indexPath.section].meal[indexPath.row])
    }
}

//MARK: UICollectionview datasource and delegate
extension WeeklyDietViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dateCell", for: indexPath)
        let dayName : UILabel = cell.viewWithTag(1) as! UILabel
        let dayNumber : UILabel = cell.viewWithTag(2) as! UILabel
        let cellDate : Date = Calendar.current.date(byAdding: .day, value: indexPath.row, to: Date())!
        let dayNameFormatter = DateFormatter()
        let dayNumberFormatter = DateFormatter()
        if selectedIndex == indexPath.item {
            dayNumber.backgroundColor = UIColor.white
            dayNumber.textColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        }
        else {
            dayNumber.backgroundColor = UIColor.clear
            dayNumber.textColor = UIColor.white
        }
        dayNumber.layer.masksToBounds = true
        dayNameFormatter.dateFormat = "EEE"
        dayNumberFormatter.dateFormat = "dd"
        dayName.text = dayNameFormatter.string(from: cellDate)
        dayNumber.text = dayNumberFormatter.string(from: cellDate)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.item
        collectionView.reloadData()
        dietTableView.reloadData()
        if (mealData[sortedDateArray![selectedIndex]]?.count)! == 0 {
            statusView.isHidden = false
        }
        else {
            statusView.isHidden = true
        }
    }
}

extension WeeklyDietViewController: SeactionHeaderClicked {
    func reloadTable(section: Int,collapsed : Bool) {
        mealData[sortedDateArray![selectedIndex]]![section].collapsed = collapsed
        dietTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: UITableViewRowAnimation.fade)
    }
}
