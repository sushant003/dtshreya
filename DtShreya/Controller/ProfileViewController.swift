//
//  ProfileViewController.swift
//  DtShreya
//
//  Created by kwikard on 10/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    @IBOutlet weak var rankImage: UIImageView!
    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var badgeImage: UIImageView!
    @IBOutlet weak var badgelabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var medalLabel: UILabel!
    @IBOutlet weak var currentBadgeLabel: UILabel!
    @IBOutlet weak var weightLossLabel: UILabel!
    @IBOutlet weak var bellRungLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var appointmentLabel: UILabel!
    
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getBasicInfo()
        statusImage.image = UIImage(named: "")
    }
    
    //MARK: Webservice Method
    func getBasicInfo() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.profileCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getProfleDataServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getProfileDataServiceSuffix, notificationKey: AppConstants.Dashboard.getProfleDataServiceNotify, controllerName: self,requestType: "")
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Net_Error
            statusImage.image = #imageLiteral(resourceName: "serverError")
        }
    }
    
    @objc func profileCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                switch (serviceData.userInfo?["badge"] as? String){
                case "1":
                    rankLabel.text = "Newbie"
                    badgelabel.text = "Can do it"
                    rankImage.image = #imageLiteral(resourceName: "rank1")
                    badgeImage.image = #imageLiteral(resourceName: "badge1")
                case "2":
                    rankLabel.text = "Senior"
                    badgelabel.text = "Enthusiastic"
                    rankImage.image = #imageLiteral(resourceName: "rank2")
                    badgeImage.image = #imageLiteral(resourceName: "badge2")
                case "3":
                    rankLabel.text = "Pro"
                    badgelabel.text = "Disciplined"
                    rankImage.image = #imageLiteral(resourceName: "rank3")
                    badgeImage.image = #imageLiteral(resourceName: "badge3")
                case "4":
                    rankLabel.text = "Super Pro"
                    badgelabel.text = "Going awesome"
                    rankImage.image = #imageLiteral(resourceName: "rank4")
                    badgeImage.image = #imageLiteral(resourceName: "badge4")
                default:
                    break
                }
                nameLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.name] as? String
                emailLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.email] as? String
                medalLabel.text = rankLabel.text
                currentBadgeLabel.text = badgelabel.text
                weightLossLabel.text = (serviceData.userInfo?[AppConstants.ServiceKeys.achieve] as? String)! + "kgs"
                bellRungLabel.text = serviceData.userInfo?[AppConstants.ServiceKeys.bells] as? String
                planLabel.text = serviceData.userInfo?["plane"] as? String
                appointmentLabel.text = "\(String(describing: serviceData.userInfo?[AppConstants.ServiceKeys.appointments] as! Int))"
            }
            else {
                serverStatusView.isHidden = false
                serverStatusLabel.text = message
                statusImage.image = #imageLiteral(resourceName: "serverError")
            }
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Server_Error
            statusImage.image = #imageLiteral(resourceName: "serverError")
        }
    }
    
    //MARK: IBAction
    
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
