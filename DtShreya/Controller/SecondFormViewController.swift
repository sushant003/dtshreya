//
//  SecondFormViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 19/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class SecondFormViewController: BaseViewController {

    @IBOutlet weak var wakeUpTF: UITextField!
    @IBOutlet weak var sleepTF: UITextField!
    @IBOutlet weak var breakfastTF: UITextField!
    @IBOutlet weak var midMorningTF: UITextField!
    @IBOutlet weak var lunchTF: UITextField!
    @IBOutlet weak var eveningTF: UITextField!
    @IBOutlet weak var lateEveningTF: UITextField!
    @IBOutlet weak var dinnerTF: UITextField!
    @IBOutlet weak var postDinnerTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: IBAction
    
    @IBAction func onClickSkipButton(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "ThirdFormViewController", controllerName: self)
    }
    
    @IBAction func onClickNextButton(_ sender: Any) {
        if AppHelper.getInstance().isNetworkAvailable() {
            let params = ["data":["dietrecallList":[AppConstants.ServiceKeys.wakeuptime:wakeUpTF.text!,AppConstants.ServiceKeys.sleepTime:sleepTF.text!,"breakfast":breakfastTF.text!,"midMorning":midMorningTF.text!,"lunch":lunchTF.text!,"evening":eveningTF.text!,"lateEvening":lateEveningTF.text!,"dinner":dinnerTF.text!,"postDinner":postDinnerTF.text!]],"clientId":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)]
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.SignupForm.formTwoServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: params as NSDictionary, url: AppConstants.SignupForm.formTwoServiceSuffix, notificationKey: AppConstants.SignupForm.formTwoServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().pushView(identifier: "ThirdFormViewController", controllerName: self)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
