//
//  FeedbackViewController.swift
//  DtShreya
//
//  Created by kwikard on 12/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import Cosmos

class FeedbackViewController: BaseViewController {

    @IBOutlet weak var feedbackTV: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var selectDiatitianModel: UIButton!
    var selectedId : Int = 0
    var dietitianModel : [DietitiansModel] = [DietitiansModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        feedbackTV.layer.cornerRadius = 5
        feedbackTV.layer.borderColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0).cgColor
        feedbackTV.layer.borderWidth = 0.5
        feedbackTV.text = "Write your comment here..."
        feedbackTV.textColor = UIColor.lightGray
        getIds()
        // Do any additional setup after loading the view.
    }
    
    
    //MARK: IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickSelectdietitian(_ sender: Any) {
        AppHelper.getInstance().pushControllerWithData(id: "SelectDietitianViewController", controllerName: self, data: ["data":dietitianModel,"controller":self])
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
        self.view.endEditing(true)
        if feedbackTV.text == "" || feedbackTV.text == "Write your comment here..." {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please write a review first.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else if selectedId == 0 {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please select a dietitian.", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else {
            giveReview()
        }
    }
    
    //MARK: Webservice Method
    func getIds() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDietitianIDServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                let resultDict: [[String:Any]] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as? [[String:Any]])!
                for item in resultDict {
                    
                   let  dietitian = DietitiansModel(name: item["name"] as! String, url: item["url"] as! String, id: item["id"] as! Int)
                    dietitianModel.append(dietitian)
                }
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func giveReview() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.giveReviewCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDietitianIDServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: ["from":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),"to":selectedId,"star":ratingView.rating,"review":feedbackTV.text!], url: AppConstants.Dashboard.giveReviewServiceSuffix, notificationKey: AppConstants.Dashboard.getDietitianIDServiceNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func giveReviewCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Review submitted successfully", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension FeedbackViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write your comment here..."
            textView.textColor = UIColor.lightGray
        }
    }
}

extension FeedbackViewController : throwData {
    func sendPopedData(data: Any) {
        selectedId = (data as! DietitiansModel).id
        selectDiatitianModel.setTitle((data as! DietitiansModel).name, for: .normal)
    }
}
