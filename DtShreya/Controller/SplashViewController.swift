//
//  SplashViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 09/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class SplashViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if "\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.isLoggedIn))" == "1" {
            checkUserStatus()
        }
        else {
            AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
        }
    }
    
    func checkUserStatus() {
        if AppHelper.getInstance().isNetworkAvailable() {
            NotificationCenter.default.addObserver(self,selector: #selector(self.catchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.checkUserStatusNotify),object: nil)
            Webservice.getInstance().hitService(parameter: ["userid":AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.checkUserStatusSuffix, notificationKey: AppConstants.Dashboard.checkUserStatusNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Sorry couldn't connect you. Please check your internet or reopen the app", cancelButtonName: "Cancel",otherButtonName: "Retry", controllerName: self,isAlertOnly: false)
        }
    }
    
    @objc func catchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().setValueForUserDefault(value: "Y", key: AppConstants.ServiceKeys.dieterStatus)
                if (serviceData.userInfo?["basic"] as? Bool)! {
                    AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
                }
                else {
                    AppHelper.getInstance().pushView(identifier: "SetupFormViewController", controllerName: self)
                }
            }
            else {
                AppHelper.getInstance().setValueForUserDefault(value: "N", key: AppConstants.ServiceKeys.dieterStatus)
                if let status = serviceData.userInfo?["basic"] {
                    if status as! Bool {
                        AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
                    }
                    else {
                        AppHelper.getInstance().pushView(identifier: "SetupFormViewController", controllerName: self)
                    }
                    return
                }
                AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}

extension SplashViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        checkUserStatus()
    }
    
    func alertNegativeButtonPressed() {
    }
}
