//
//  DashboardViewController.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 19/02/18.
//  Copyright © 2018 Medicians. All rights reserved.
//
import UIKit

class DashboardViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var firstContainerview: CustomView!
    @IBOutlet weak var secondContainerView: CustomView!
    @IBOutlet weak var dietitianName: UILabel!
    @IBOutlet weak var daysLeftLabel: UILabel!
    @IBOutlet weak var tergetLabel: UILabel!
    @IBOutlet weak var achievedLabel: UILabel!
    @IBOutlet weak var timePeriodLabel: UILabel!
    @IBOutlet weak var bellsLabel: UILabel!
    
    @IBOutlet weak var nutritionistTipsPageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cvFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var serverStatusView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var serverStatusLabel: UILabel!
    
    @IBOutlet weak var boxOne: CustomView!
    @IBOutlet weak var boxTwo: CustomView!
    @IBOutlet weak var boxThree: CustomView!
    @IBOutlet weak var boxFour: CustomView!
    
    var serverDataModel : DashboardModel = DashboardModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        statusImage.image = UIImage.init(named: "")
        cvFlowLayout.itemSize = CGSize(width: self.view.frame.width - 20, height: 300)
        getHomeData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstContainerview.createShadow()
        secondContainerView.createShadow()
        boxOne.createShadow()
        boxTwo.createShadow()
        boxThree.createShadow()
        boxFour.createShadow()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        scrollCV()
    }
    
    func scrollCV() {
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
    }

    @objc func scrollAutomatically(_ timer1: Timer) {
        if let coll  = collectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  < 4 - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
            }
        }
    }
    
    //MARK: IBAction
    @IBAction func onClickReadMore(_ sender: UIButton) {
//        let indexPath = collectionView.indexPath(for: sender.superview?.superview?.superview as! UICollectionViewCell)!
        UIApplication.shared.open(URL(string : (sender.tag == 2 ? serverDataModel.activities["bloglink"]! : serverDataModel.health["bloglink"]!))!, options: [:], completionHandler: { (status) in
        })
    }
    
    //MARK: Webservice Method
    func getHomeData() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.serverStatusView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.homeCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.getDashboardDataServiceNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.meal:AppHelper.getInstance().getFoodTime(),AppConstants.ServiceKeys.clientID:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId)], url: AppConstants.Dashboard.getDashboardDataServiceSuffix, notificationKey: AppConstants.Dashboard.getDashboardDataServiceNotify, controllerName: self,requestType: "")
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Net_Error
            statusImage.image = #imageLiteral(resourceName: "serverError")
        }
    }
    
    @objc func homeCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.serverStatusView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                serverStatusView.isHidden = true
                self.prepareHomeData(serverDict : serviceData.userInfo as! [String:Any])
                collectionView.dataSource = self
                collectionView.delegate = self
                collectionView.reloadData()
            }
            else {
                serverStatusView.isHidden = false
                serverStatusLabel.text = message
                statusImage.image = #imageLiteral(resourceName: "serverError")
            }
        }
        else {
            serverStatusView.isHidden = false
            serverStatusLabel.text = AppConstants.ErrorMessags.Server_Error
            statusImage.image = #imageLiteral(resourceName: "serverError")
        }
    }
    
    func prepareHomeData(serverDict : [String:Any]) {
        serverDataModel.achive = serverDict[AppConstants.ServiceKeys.achieve] as! String
        serverDataModel.hasmeal = !(serverDict[AppConstants.ServiceKeys.hasmeal] is Int) ? Int(serverDict[AppConstants.ServiceKeys.hasmeal] as! String)! : (serverDict[AppConstants.ServiceKeys.hasmeal] as! Int)
        serverDataModel.target = serverDict[AppConstants.ServiceKeys.target] as! String
        serverDataModel.bells = serverDict[AppConstants.ServiceKeys.bells] as! String
        serverDataModel.days = serverDict[AppConstants.ServiceKeys.days] as! String
        serverDataModel.timeperiod = serverDict[AppConstants.ServiceKeys.timeperiod] as! Int
        serverDataModel.hasappointment = serverDict[AppConstants.ServiceKeys.hasappointment] as! Int
        serverDataModel.nextappoint = serverDataModel.hasappointment == 1 ? (serverDict[AppConstants.ServiceKeys.nextappoint] as! String) : ""
        serverDataModel.address = serverDataModel.hasappointment == 1 ? (serverDict[AppConstants.ServiceKeys.address] as! String) : ""
        
        serverDataModel.dietitian = serverDict[AppConstants.ServiceKeys.dietitian] as! [String]
        serverDataModel.activities = serverDict[AppConstants.ServiceKeys.activities] as! [String:String]
        serverDataModel.health = serverDict[AppConstants.ServiceKeys.health] as! [String:String]
        
        serverDataModel.nextMeal = serverDataModel.hasmeal == 1 ? ((serverDict[AppConstants.ServiceKeys.nextmeal] is String) ? createArrayFromString(string: serverDict[AppConstants.ServiceKeys.nextmeal] as! String) : serverDict[AppConstants.ServiceKeys.nextmeal] as! [String]) : ["No upcoming meal"]
        
        setData()
    }
    
    func setData() {
        dietitianName.text = serverDataModel.dietitian.joined(separator: ",")
        daysLeftLabel.text = serverDataModel.days
        tergetLabel.text = serverDataModel.target + " Kg"
        achievedLabel.text = serverDataModel.achive + " Kg"
        timePeriodLabel.text = "\(serverDataModel.timeperiod) weeks"
        bellsLabel.text = serverDataModel.bells
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = Double(Float(scrollView.contentOffset.x) / Float(scrollView.frame.width))
        self.nutritionistTipsPageControl.currentPage = Int(round(contentOffset))
    }
    
    func createArrayFromString(string : String) -> [String]{
        let data = string.data(using: .utf8)!
        do {
            if (try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String]) != nil
            {
                var arr = (try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String])
                arr = arr?.map{ $0.replacingOccurrences(of: "\n", with: "")}
                arr = arr?.map{ $0.replacingOccurrences(of: "\r", with: "")}
                return arr!
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }
        return []
    }
}

extension DashboardViewController : UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 || indexPath.item == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
            (cell.viewWithTag(1) as! UILabel).text = indexPath.item == 0 ? "Upcoming Meal" : "Appointment"
            (cell.viewWithTag(3) as! UILabel).text = indexPath.item == 0 ? serverDataModel.nextMeal.joined(separator: ",") : ((serverDataModel.nextappoint.components(separatedBy: " ")[0].count == 0 ? "No appointment" : serverDataModel.nextappoint.components(separatedBy: " ")[0]))
            (cell.viewWithTag(2) as! UIImageView).image = indexPath.row == 0 ? #imageLiteral(resourceName: "dashboard_meal") : #imageLiteral(resourceName: "dashboard_appointment")
            (cell.viewWithTag(4) as! UILabel).text = indexPath.item == 0 ? AppHelper.getInstance().getFoodTimeFromNumber(mealNumber: AppHelper.getInstance().getFoodTime()) : ((serverDataModel.nextappoint.count == 0) ? "-" : serverDataModel.nextappoint.components(separatedBy: " ")[1])
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
            (cell.viewWithTag(2) as! UILabel).text = (indexPath.row == 2 ? "Activies" : "Health Tips")
            AppHelper.getInstance().loadImageIntoImageView(imageUrl: (indexPath.item == 2 ? serverDataModel.activities["imagelink"]! : serverDataModel.health["imagelink"]!), imageView: (cell.viewWithTag(3) as! UIImageView), placeHolder: "")
            (cell.viewWithTag(4) as! UILabel).text = (indexPath.item == 2 ? serverDataModel.activities["title"] : serverDataModel.health["title"])
            cell.subviews[0].subviews[0].subviews[3].tag = indexPath.item
            return cell
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.superview!.superview! == collectionView {
            nutritionistTipsPageControl.currentPage  = Int(scrollView.contentOffset.x / self.view.frame.width)
        }
    }
}
