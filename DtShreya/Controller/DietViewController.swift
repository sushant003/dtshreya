//
//  CreateDietViewController.swift
//  Meditician
//
//  Created by Sushant Jugran on 10/07/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class DietViewController: BaseViewController {
    
    @IBOutlet weak var dietTableView: UITableView!
    @IBOutlet weak var statusView: UIView!
    var mealData : [MealModel] =  [MealModel]()
    var notes : [String] = [String]()
    var recipeName : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statusView.isHidden = true
        let headerNib = UINib.init(nibName: "HeaderView", bundle: Bundle.main)
        dietTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "HeaderView")
        dietTableView.sectionFooterHeight = 5.0
        getDietOfSevenDays()
    }
    
    
    func getDietOfSevenDays() {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getDietCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.userId:AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId),AppConstants.ServiceKeys.date:formatter.string(from: Date())], url: AppConstants.Clients.get_diet_service_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getDietCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                statusView.isHidden = true
                let resultDict: [String:Any] = (serviceData.userInfo?[AppConstants.ServiceKeys.result] as! [String:Any])
                prepareTableData(serviceData: resultDict)
                dietTableView.dataSource = self
                dietTableView.delegate = self
                dietTableView.reloadData()
            }
            else {
                statusView.isHidden = false                
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    func prepareTableData(serviceData : [String:Any]) {
        for (key,value) in serviceData  {
            var meal : MealModel!
            if key == AppConstants.ServiceKeys.notes {
                notes = value as! [String]
                continue
            }
            switch key {
            case AppConstants.ServiceKeys.firstMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.firstMealName, meal: value as! [String], mealIndex: 0)
            case AppConstants.ServiceKeys.secondMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.secondMealName, meal: value as! [String], mealIndex: 1)
            case AppConstants.ServiceKeys.thirdMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.thirdMealName, meal: value as! [String], mealIndex: 2)
            case AppConstants.ServiceKeys.fourthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fourthMealName, meal: value as! [String], mealIndex: 3)
            case AppConstants.ServiceKeys.fifthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.fifthMealName, meal: value as! [String], mealIndex: 4)
            case AppConstants.ServiceKeys.sixthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.sixthMealName, meal: value as! [String], mealIndex: 5)
            case AppConstants.ServiceKeys.seventhMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.seventhMealName, meal: value as! [String], mealIndex: 6)
            case AppConstants.ServiceKeys.eighthMeal:
                meal = MealModel(name: AppConstants.ServiceKeys.eightMealName, meal: value as! [String], mealIndex: 7)
            case AppConstants.ServiceKeys.supplements:                
                    meal = MealModel(name: AppConstants.ServiceKeys.supplementsName, meal: value as! [String], mealIndex: 8)
                
            default:
                break
            }
            if key != "foodnine" {
                mealData.append(meal!)
            }
        }
        mealData = mealData.sorted(by: { (first, second) -> Bool in
            first.index < second.index
        })
    }
    
    func getRecepie(foodName : String) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.dietTableView)
            NotificationCenter.default.addObserver(self,selector: #selector(self.getRecepieCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Clients.get_diet_service_notify),object: nil)
            
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.name:foodName], url: AppConstants.Clients.get_recepie_service_suffix, notificationKey: AppConstants.Clients.get_diet_service_notify, controllerName: self, requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func getRecepieCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.dietTableView)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                AppHelper.getInstance().showAlert(alertTitle: "Recipe", message: (serviceData.userInfo?["recipe"] as? String) == nil ? "N/A" : (serviceData.userInfo?["recipe"] as? String)!, cancelButtonName: "OK", otherButtonName: "Copy", controllerName: self, isAlertOnly: false)
                recipeName = (serviceData.userInfo?["recipe"] as? String) == nil ? "N/A" : (serviceData.userInfo?["recipe"] as? String)!
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    //MARK: IBAction
    @IBAction func onClickNotes(_ sender: Any) {        
        AppHelper.getInstance().pushControllerWithData(id: "DietNotesViewController", controllerName: self, data: notes)
    }
}

extension DietViewController : alertPositiveButtonDelegate {
    func alertNegativeButtonPressed() {
    }
    
    func alertPositiveButtonPressed() {
        UIPasteboard.general.string = recipeName
    }
}

//MARK: Tableview datasource and delegate
extension DietViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return mealData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mealData[section].collapsed ? 0 : mealData[section].meal.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.delegate = self
        headerView.seactionIndex = section
        headerView.isCollapsed = mealData[section].collapsed
        headerView.headerLabel.text = mealData[section].mealTimeName
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCell", for: indexPath)
        (cell.viewWithTag(1) as! UILabel).text = mealData[indexPath.section].meal[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        getRecepie(foodName: mealData[indexPath.section].meal[indexPath.row])
    }
}

extension DietViewController: SeactionHeaderClicked {
    func reloadTable(section: Int,collapsed : Bool) {
        mealData[section].collapsed = collapsed
        dietTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: UITableViewRowAnimation.fade)
    }
}
