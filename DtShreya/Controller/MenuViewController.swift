//
//  MenuViewController.swift
//  GDI
//
//  Created by Sushant Jugran on 03/07/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var headerlabel: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet var headerView: UIView!
    var menuArray : [Int:[[String:UIImage]]] = [0:[[AppConstants.Menu.tests:#imageLiteral(resourceName: "menu_test")],[AppConstants.Menu.dietDiary:#imageLiteral(resourceName: "diary")],[AppConstants.Menu.feedback:#imageLiteral(resourceName: "menu_feedback")],[AppConstants.Menu.healthTips:#imageLiteral(resourceName: "activity")]],1:[[AppConstants.Menu.contactUs:#imageLiteral(resourceName: "menu_contact")],[AppConstants.Menu.logout:#imageLiteral(resourceName: "menu_logout")]]]
    var nonActiveMenuArray : [Int:[[String:UIImage]]] = [0:[[AppConstants.Menu.facebook:#imageLiteral(resourceName: "facebook")],[AppConstants.Menu.twitter:#imageLiteral(resourceName: "twitter")],[AppConstants.Menu.instagram:#imageLiteral(resourceName: "instagram")],[AppConstants.Menu.youtube:#imageLiteral(resourceName: "youtube")],[AppConstants.Menu.logout:#imageLiteral(resourceName: "menu_logout")]]]
    var selectedIndex : [Int:Int] = [0:0]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.userName) as? String
        email.text = AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.email) as? String
    }
    
    func isValidAccount() -> Bool {
        if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.dieterStatus) as! String) == "Y" {
            return true
        }
        return false
    }
}

extension MenuViewController : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isValidAccount() {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if isValidAccount() {
            if section == 0 {
                return 0
            }
            else {
                return 40
            }
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if isValidAccount() {
            if section == 0 {
                return nil
            }
            else {
                return headerView
            }
        }
        headerlabel.text = "Connect to Us"
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isValidAccount() {
            return (menuArray[section]?.count)!
        }
        return (nonActiveMenuArray[section]?.count)!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.selectionStyle = .none
        (cell.viewWithTag(2) as! UILabel).text = isValidAccount() ? menuArray[indexPath.section]?[indexPath.row].first?.key : nonActiveMenuArray[indexPath.section]?[indexPath.row].first?.key
        if selectedIndex.first?.key == indexPath.section  &&  selectedIndex.first?.value == indexPath.row{
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        }
        else {
            (cell.viewWithTag(2) as! UILabel).textColor = UIColor.black
        }
        (cell.viewWithTag(1) as! UIImageView).image = (cell.viewWithTag(1) as! UIImageView).image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        (cell.viewWithTag(1) as! UIImageView).tintColor = UIColor(red: 230.0/255.0, green: 27.0/255.0, blue: 88.0/255.0, alpha: 1.0)
        (cell.viewWithTag(1) as! UIImageView).image = isValidAccount() ? menuArray[indexPath.section]?[indexPath.row].first?.value : nonActiveMenuArray[indexPath.section]?[indexPath.row].first?.value
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = [indexPath.section:indexPath.row]
        tableView.reloadData()
        let str = isValidAccount() ? menuArray[indexPath.section]![indexPath.row].first?.key : nonActiveMenuArray[indexPath.section]![indexPath.row].first?.key
        switch str {
        case AppConstants.Menu.tests:
            AppHelper.getInstance().pushControllerWithData(id: "AppointmentViewController", controllerName: self, data: false)
        case AppConstants.Menu.dietDiary :
            AppHelper.getInstance().pushControllerWithData(id: "DiaryViewController", controllerName: self, data: false)
        case AppConstants.Menu.healthTips :
            AppHelper.getInstance().pushView(identifier: "TabBar1ViewController", controllerName: self)
        case AppConstants.Menu.feedback :
            AppHelper.getInstance().pushView(identifier: "FeedbackViewController", controllerName: self)
        case AppConstants.Menu.contactUs:
            AppHelper.getInstance().pushView(identifier: "ContactUsViewController", controllerName: self)
        case AppConstants.Menu.logout:
            
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Are you sure you want to logout?", cancelButtonName: "NO", otherButtonName: "YES", controllerName: self, isAlertOnly: false)
            
            //AppHelper.getInstance().saveDeviceToken(controller: self)
        case AppConstants.Menu.facebook:
            UIApplication.shared.open(URL(string : "https://www.facebook.com/DietitianShreya/")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.twitter:
            UIApplication.shared.open(URL(string : "https://mobile.twitter.com/DietitianShreya")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.instagram:
            UIApplication.shared.open(URL(string : "https://www.instagram.com/dietitianshreya")!, options: [:], completionHandler: { (status) in
            })
        case AppConstants.Menu.youtube:
            UIApplication.shared.open(URL(string : "https://www.youtube.com/channel/UCRP40I-Avt-flLYNt5hYlrA")!, options: [:], completionHandler: { (status) in
            })
        default:
            break
        }
    }
}

extension MenuViewController : alertPositiveButtonDelegate {
    func alertPositiveButtonPressed() {
        AppHelper.getInstance().pushView(identifier: "LoginViewController", controllerName: self)
        AppHelper.getInstance().logOut()
    }
    
    func alertNegativeButtonPressed() {        
    }
}
