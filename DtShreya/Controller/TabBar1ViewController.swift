//
//  TabBar1ViewController.swift
//  DtShreya
//
//  Created by Sushant Jugran on 08/08/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class TabBar1ViewController: BaseViewController {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var tabbarItem: UITabBar!
    @IBOutlet weak var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tabbarItem.tintColor = UIColor(displayP3Red: 230/255.0, green: 27/255.0, blue: 88/255.0, alpha: 1.0)
        tabbarItem.selectedItem = (tabbarItem.items?[0])!
        tabBar(tabbarItem, didSelect: tabbarItem.selectedItem!)
        // Do any additional setup after loading the view.
    }
    
    //IBAction
    @IBAction func onClickBackButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func addViewController(controller: BaseViewController) {
        for contrlr in childViewControllers {
            if(contrlr == controller) {
                return
            }
            else {
                contrlr.removeFromParentViewController()
            }
        }
        for view in containerView.subviews {
            view.removeFromSuperview()
        }
        
        addChildViewController(controller)
        controller.view.frame = containerView.bounds
        containerView.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
}

extension TabBar1ViewController : UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 0:
            headerLabel.text = "Activity"
            let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
            controller.data = 0
            self.addViewController(controller: controller)
        case 1:
            headerLabel.text = "Blog"
            let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
            controller.data = 1
            addViewController(controller: controller)
        case 2:
            headerLabel.text = "Tips"
            let controller = AppHelper.getInstance().GetViewControllerIntance(id: "NonActiveViewController", controllerName: self) as! BaseViewController
            controller.data = 2
            addViewController(controller: controller)
        default:
            break
        }
    }
}
