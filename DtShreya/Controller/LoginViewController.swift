//
//  LoginViewController.swift
//  DtShreya
//  Created by Shlok Kapoor on 08/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBAction func signInButtonTapped(_ sender: Any) {
        self.view.endEditing(true)
        let email = usernameTextField.text
        let password = passwordTextField.text
        if password == nil || email == nil {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "Please enter all the details", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        if !AppHelper.getInstance().isValidEmail(testStr: usernameTextField.text!) {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Email_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        else if (passwordTextField?.text?.count)! < 6 {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Password_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        let params = ["email": email!, "password": password!]
        if !AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            return
        }
        DispatchQueue.main.async {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
        }
        DataService.ds.postRequest(urlExt: "login/", params: params) { (data, error) in
            if let data = data {
                print(data)
                let message  = data[AppConstants.ServiceKeys.message] as? String
                if ((data[AppConstants.ServiceKeys.status] as? Bool)!) == true {
                    
                    DispatchQueue.main.async {
                        self.saveLopinData(dict: data)
                        self.loginToFirebase()
                    }
                }
                else {
                    DispatchQueue.main.async {
                        AppHelper.getInstance().hideProgressIndicator(view: self.view)
                        AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: "\(message!)", cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                    }
                }
            }
            else {
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                    AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
                }
            }
        }
    }
    
    func saveLopinData(dict : [String:Any]) {
        AppHelper.getInstance().setValueForUserDefault(value: "1", key: AppConstants.userDefaultConstants.isLoggedIn)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.username]!, key: AppConstants.userDefaultConstants.userName)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.userId]!, key:  AppConstants.ServiceKeys.userId)
        AppHelper.getInstance().setValueForUserDefault(value: dict[AppConstants.ServiceKeys.dieterStatus]!, key: AppConstants.ServiceKeys.dieterStatus)
        AppHelper.getInstance().setValueForUserDefault(value: usernameTextField.text!, key: AppConstants.ServiceKeys.email)
    }
    
    @IBAction func signUpButtonTapped(_ sender: Any) {
        AppHelper.getInstance().pushView(identifier: "SignUpViewController", controllerName: self)
    }
    @IBAction func forgotPasswordButtonTapped(_ sender: Any) {
        AppHelper.getInstance().alertWithTextField(title: "Forgot password", placeholder: "Add email id here", text: "", controllerName: self, leftButtonTite: "Send")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
//        usernameTextField.text = "admin@dietitianshreya.com"
//        passwordTextField.text = "Password1"
    }

    //MARK: Firebase methods
    func loginToFirebase() {
        Auth.auth().signIn(withEmail: self.usernameTextField.text!, password: self.passwordTextField.text!) { (user, error) in
            if error == nil {
                AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                    if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.user_device_token) as? String)  != nil {                        
                    }
                }
            }
            else {
                self.createFirebaseAccount()
            }
        }
    }
    
    func createFirebaseAccount() {
        Auth.auth().createUser(withEmail: usernameTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                AppHelper.getInstance().pushView(identifier: "CustomMenuViewController", controllerName: self)
                DispatchQueue.main.async {
                    AppHelper.getInstance().hideProgressIndicator(view: self.view)
                    if (AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.userDefaultConstants.user_device_token) as? String)  != nil {
                        
                    }
                }
            }
            else {
                AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: (error?.localizedDescription)!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
            }
        }
    }
}

extension LoginViewController : AlertTextDelegate {
    func getInputText(text: String) {
        forgetPassword(email: text)
    }
    
    //MARK: Webservice methods
    func forgetPassword(email : String) {
        if AppHelper.getInstance().isNetworkAvailable() {
            AppHelper.getInstance().showProgressIndicator(view: self.view)
            NotificationCenter.default.addObserver(self,selector: #selector(self.passwordCatchNotification),name: NSNotification.Name(rawValue: AppConstants.Dashboard.forgetPasswordNotify),object: nil)
            Webservice.getInstance().hitService(parameter: [AppConstants.ServiceKeys.email:email], url: AppConstants.Dashboard.forgetPasswordSuffix, notificationKey: AppConstants.Dashboard.forgetPasswordNotify, controllerName: self,requestType: "")
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Net_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
    
    @objc func passwordCatchNotification(serviceData:Notification) {
        AppHelper.getInstance().hideProgressIndicator(view: self.view)
        if(serviceData.userInfo != nil) {
            let message  = serviceData.userInfo?[AppConstants.ServiceKeys.message] as? String
//            if ((serviceData.userInfo?[AppConstants.ServiceKeys.status] as? Bool)!) == true {
//                
//            }
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: message!, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
        else {
            AppHelper.getInstance().showAlert(alertTitle: AppConstants.AppName, message: AppConstants.ErrorMessags.Server_Error, cancelButtonName: "OK", otherButtonName: "", controllerName: self, isAlertOnly: true)
        }
    }
}
