//
//  DataService.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 05/04/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation

class DataService {
    static var ds = DataService()
    var baseUrlString = "https://shreyaapi.herokuapp.com/"
    var urlExt: String
    var params: [(String, String)]?
    
    init () {
        urlExt = ""
        params = []
    }
    
    init (urlExt: String, params: [(String, String)]) {
        self.urlExt = urlExt
        self.params = params
    }

    func postRequest(urlExt: String, params: Dictionary<String, Any>?, onCompletion: @escaping (_ json: Dictionary<String, Any>?, _ error: Error?) -> Void) {
        let urlString = self.baseUrlString + urlExt

        if let params = params {

            
            guard let url = URL(string: urlString) else { return }

            var request = URLRequest(url: url)

            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            print(params)
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions()) else { return }
            let convertedString = String(data: httpBody, encoding: String.Encoding.utf8)
            request.httpBody = convertedString?.data(using: .utf8)
            let session = URLSession.shared
            let task = session.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors

                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")

                    return
                }

                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")

                do {
                    let dictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    if let myDictionary = dictionary
                    {
                        onCompletion(myDictionary, nil)
                    }
                } catch {
                    onCompletion(nil, error)
                }


            }
            task.resume()
        }
    }
    
    func postRequestJSONArray(urlExt: String, params: Dictionary<String, Any>?, onCompletion: @escaping (_ json: [[String: Any]]?, _ error: Error?) -> Void) {
        let urlString = self.baseUrlString + urlExt
        
        if let params = params {
            
            
            guard let url = URL(string: urlString) else { return }
            
            var request = URLRequest(url: url)
            
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            print(params)
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions()) else { return }
            let convertedString = String(data: httpBody, encoding: String.Encoding.utf8)
            
            request.httpBody = convertedString?.data(using: .utf8)
            
            let session = URLSession.shared
            let task = session.dataTask(with: request) { data, response, error in
                
                guard let data = data, error == nil else {
                    // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                    // check for http errors
                    
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                    
                    return
                }
                
                let responseString = String(data: data, encoding: .utf8)
                print("responseString = \(String(describing: responseString))")
                do {
                    let dictionary = try JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions()]) as? [[String: Any]]
                    if let myDictionary = dictionary {
                        onCompletion(myDictionary, nil)
                    }
                    else {
                    }
                } catch {
                    onCompletion(nil, error)
                }
                
                
            }
            task.resume()
        }
    }
    
    
}

