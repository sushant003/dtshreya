//
//  Webservice.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 16/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit
import Alamofire
import Foundation

class Webservice: NSObject {
    
    static var sharedInstance: Webservice! = nil
    static func getInstance() -> Webservice {
        if(sharedInstance == nil) {
            sharedInstance = Webservice()
        }
        return sharedInstance
    }
    
    func hitService(parameter:NSDictionary!,url:String,notificationKey:String , controllerName : UIViewController,requestType : String)  {
        Alamofire.request(AppConstants.mainUrl.appending(url), method: (requestType == "get" ? HTTPMethod.get : HTTPMethod.post), parameters: parameter as? Parameters, encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseJSON {
            (response) in
            if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: response.result.value as! [String : Any])
            }
            else {
                print(response.error?.localizedDescription ?? "")
                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: nil)
            }
            NotificationCenter.default.removeObserver(controllerName, name: NSNotification.Name(rawValue: notificationKey), object: nil)
        }
    }
    
    func hit(parameter:NSDictionary!,url:String,notificationKey:String , controllerName : UIViewController,requestType : String)  {
        Alamofire.request(AppConstants.mainUrl.appending(url), method: (requestType == "get" ? HTTPMethod.get : HTTPMethod.post), parameters: parameter as? Parameters, encoding: JSONEncoding.default,headers: nil).validate(contentType: ["text/html"]).responseString {
            (response) in
            if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: response.result.value as! [String : Any])
            }
            else {
                print(response.error?.localizedDescription ?? "")
                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: nil)
            }
            NotificationCenter.default.removeObserver(controllerName, name: NSNotification.Name(rawValue: notificationKey), object: nil)
        }
    }
    
//    func hitServiceWithObject(parameter:NSDictionary!,url:String,notificationKey:String , controllerName : UIViewController,requestType : String)  {
//        Alamofire.request(AppConstants.mainUrl.appending(url), method: (requestType == "get" ? HTTPMethod.get : HTTPMethod.post), parameters: parameter as? Parameters, encoding: JSONEncoding.default,headers: nil).validate(contentType: ["application/json"]).responseString {
//            (response) in
//            if(response.result.value != nil && Alamofire.JSONSerialization.isValidJSONObject(response.result.value!)) {
//                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: response.result.value as! [String : Any])
//            }
//            else {
//                print(response.error?.localizedDescription ?? "")
//                NotificationCenter.default.post(name: Notification.Name(notificationKey), object: nil, userInfo: nil)
//            }
//            NotificationCenter.default.removeObserver(controllerName, name: NSNotification.Name(rawValue: notificationKey), object: nil)
//        }
//    }
    
    func stringToJsonParser(data:String) -> [String:Any] {
        if let data = data.data(using: .utf8) {
            do {
                return   try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as! [String : Any]
            } catch {
                print(error.localizedDescription)
                return [:]
                
            }
            
        }
        return [:]
    }
    
}
