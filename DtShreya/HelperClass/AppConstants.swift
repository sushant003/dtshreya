//
//  AppConstants.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 16/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import Foundation

struct AppConstants {
    static var mainUrl = "http://shreyaapi.herokuapp.com/"
    static let AppName = "Eat Right Diet"
    static let deviceType = "ios"
    static let GOOGLE_API_KEY = "236309951172-u78crnb5nj8084k6jhkjk8227tljseeh.apps.googleusercontent.com"    
    
    struct Menu {
        static let tests = "Tests"
        static let dietDiary = "Diet Diary"
        static let healthTips = "Health Read"
        static let feedback = "Feedback"
        static let contactUs = "Contact Us"
        static let logout = "Log Out"
        static let facebook = "Facebook"
        static let twitter = "Twitter"
        static let instagram = "Instagram"
        static let youtube = "Youtube"
    }
    
    struct SignupForm {
        static let formOneServiceSuffix = "savebasicinfo/"
        static let formOneServiceNotify = "savebasicinfoNotification"
        static let formTwoServiceSuffix = "savedietrecall/"
        static let formTwoServiceNotify = "savedietrecallNotification"
        static let formThreeServiceSuffix = "savefoodspecification/"
        static let formThreeServiceNotify = "savedietrecallNotification"
    }
    
    struct Dashboard {
        static let checkUserStatusSuffix = "userloginstatus/"
        static let checkUserStatusNotify = "userloginstatusNotify"
        static let getDashboardDataServiceSuffix = "userdashboard/"
        static let getDashboardDataServiceNotify = "userDashboardNotification"
        static let getProfileDataServiceSuffix = "userbasicinfo/"
        static let getProfleDataServiceNotify = "userProfileNotification"
        static let getDietitianIDServiceSuffix = "dietitianids/"
        static let getDietitianIDServiceNotify = "getDietitianIDServiceNotify"
        static let giveReviewServiceSuffix = "savereview/"
        static let getDashboardCategorySuffix = "getcategorydata/"
        static let getDashboardCategoryNotify = "getcategorydataNotification"
        static let forgetPasswordSuffix = "password/"
        static let forgetPasswordNotify = "forgetPasswordNotification"
    }
    
    struct Appointments {
        static let appointment_list_service_suffix = "getuserappointment/"
        static let appointment_list_service_notify = "appointmentListNotification"
        static let reschedule_appointment_suffix = "sendreschedulerequest/"
        static let reschedule_appointment_service_notify = "sendrescheduleRequestNotification"
        static let test_list_service_suffix = "getusertests/"
        static let create_diary_service_suffix = "creatediary/"
        static let create_diary_service_notify = "creatediaryNotify"
    }
    
    
    struct Clients {
        static let client_list_service_suffix = "getclients/"
        static let client_list_service_notify = "clientListNotification"
        
        static let shopping_list_service_suffix = "shopping/"
        static let shopping_list_service_notify = "shoppingListNotification"
        
        static let get_meal_service_suffix = "getallfood/"
        static let get_meal_service_notify = "getMealNotification"
        
        static let get_notes_service_suffix = "getnote/"
        static let get_notes_service_notify = "getNoteNotification"
        
        static let get_appointment_service_suffix = "getuserappointment/"
        static let get_appointment_service_notify = "getAppointmentNotification"        
        
        static let get_date_diet_service_suffix = "getdatediet/"
        static let get_date_diet_service_notify = "getDateDietNotification"
        
        static let get_diet_service_suffix = "getdatediet/"
        static let get_diet_service_notify = "getDietNotification"
        
        static let get_recepie_service_suffix = "recipes/"
        
        
        static let get_seven_diet_service_suffix = "upcomingdiet/"
        static let get_seven_service_notify = "upcomingdiettNotification"
        
        static let get_diary_service_suffix = "getdatediary/"
        static let get_diary_service_notify = "getDiaryNotification"
        
        static let get_profile_service_suffix = "getprofiledata/"
        static let get_profile_service_notify = "getProfileNotification"
        
        static let save_profile_service_suffix = "saveprofiledata/"
        static let save_profile_service_notify = "saveProfileNotification"
        
        static let get_disease_service_suffix = "getdiseases/"
        static let get_disease_service_notify = "getDiseasesNotification"
        
        static let get_template_name_service_suffix = "templatename/"
        static let get_template_name_service_notify = "getTemplateNameNotification"
        
        static let get_template_data_service_suffix = "getdiettemplate/"
        static let get_template_data_service_notify = "getTemplateDataNotification"
        
        static let get_progress_service_suffix = "getprogress/"        
        static let get_progress_service_notify = "getProgressNotification"
        
        static let ring_bell_service_suffix = "ringbell/"
        static let ring_bell_service_notify = "ringbellNotification"
        
    }
    
    struct userDefaultConstants {
        static let dietitian_id = "dietitianId"        
        static let device_id = "device_id"
        static let clinic_id = "clinicId"
        static let user_access_token = "accessToken"
        static let user_device_token = "device_token"
        static let isLoggedIn = "isLoggedIn"
        static let userName = "username"
        static let email = "email"
        static let profileImage = "url"
    }
    
    struct ServiceKeys {
        static let message = "msg"
        static let status = "res"
        static let result = "response"        
        static let id = "id"
        static let email = "email"
        static let password = "password"
        static let dietitianId = "dietitianId"
        static let appointmentId = "appointmentId"
        static let hasappointment = "hasappointment"
        static let nextappoint = "nextappoint"
        static let nextmeal = "nextmeal"
        static let userId = "userId"
        static let timeperiod = "timeperiod"
        static let clinicID = "clinicId"
        static let clientID = "clientid"
        static let name = "name"
        static let exempt = "exempt"
        static let phone = "phone"
        static let programId = "programId"
        static let daysPassed = "dayspassed"
        static let daysleft = "daysleft"
        static let requestedTime = "requestedtime"
        static let startDate = "startdate"
        static let createIn = "createIn"
        static let time = "time"
        static let type = "type"
        static let plan = "plan"
        static let kgsLost = "kgslost"
        static let username = "username"        
        static let date = "date"
        static let action = "action"
        static let health = "health"
        static let activities = "Activities"
        static let achieve = "achive"
        static let dietData = "dietdata"
        static let dietDate = "dietdate"
        static let appointmentStatus = "status"
        static let dieterStatus = "status"    
        static let dietitian = "dietitian"
        static let dateOfDiet = "dateofdiet"        
        static let note = "note"
        static let notes = "notes"
        static let weight = "weight"
        static let bone = "bone"
        static let bells = "bells"
        static let appointments = "appointments"
        static let days = "days"
        static let water = "water"
        static let fat = "fat"
        static let muscle = "muscle"
        static let testName = "testname"
        static let meal = "meal"
        static let firstMeal = "foodone"
        static let secondMeal = "foodtwo"
        static let thirdMeal = "foodthree"
        static let fourthMeal = "foodfour"
        static let fifthMeal = "foodfive"
        static let sixthMeal = "foodsix"
        static let seventhMeal = "foodseven"
        static let eighthMeal = "foodeight"
        static let supplements = "supplements"
        
        static let firstMealName = "Early Morning"
        static let secondMealName = "Breakfast"
        static let thirdMealName = "Mid Morning"
        static let fourthMealName = "Lunch"
        static let fifthMealName = "Evening"
        static let sixthMealName = "Late Evening"
        static let seventhMealName = "Dinner"
        static let eightMealName = "Post Dinner"        
        static let supplementsName = "Supplements"
        
        static let food = "food"
        static let rfood = "rfood"
        static let sleepTime = "sleeptime"
        static let wakeuptime = "wakeuptime"
        static let pooped = "pooped"
        static let greenTea = "greentea"
        static let waterIntake = "waterintake"
        static let todayWeight = "todayweight"
        static let diet_data = "diet_data"
        //GeProfile constants
        static let medicalHistory = "medicalhistory"
        static let diabetes = "diabetes"
        static let vitb12 = "vitb12"
        static let lastLeastWeight = "lastleastweight"
        static let periods = "periods"
        static let weightManage = "weightmanage"
        static let anyDisease = "anydisease"
        static let sgot = "sgot"
        static let medication = "medication"
        static let hypothyo = "hypothyo"
        static let cholestrol = "cholestrol"
        static let thinnerBlood = "thinnerblood"
        static let bloodGroup = "bloodgroup"
        static let vitaminD3 = "vitamind3"
        static let lactation = "lactation"
        static let bp = "bp"
        static let hb = "hb"
        static let uricacid = "uricacid"
        static let calcium = "calcium"
        static let hba1c = "hba1c"
        
        static let basicinfo = "basicinfo"
        static let height = "height"
        static let gender = "gender"
        static let whatsapp = "whatsapp"
        static let hasmeal = "hasmeal"
        static let target = "target"
        static let address = "address"
        static let referredBy = "referredby"
        static let contact = "contact"
        static let anniversary = "anniversary"
        static let occupation = "occupation"
        static let dob = "dob"
        static let foodSpecification = "foodspecification"
        static let likes = "likes"
        static let dislikes = "dislikes"
        static let vegnonveg = "vegnonveg"
        static let allergies = "allergies"
       
        static let templateName = "templatename"
        static let templateId = "templateId"
    }
    
    struct ErrorMessags {
        static let Server_Error = "Can't connect to server, please try after some time."
        static let Net_Error = "Can't connect to internet. Check your network connection."
        static let failed_service_Error = "Something unexpected happened."
        static let Email_Error = "Please enter a valid email address"
        static let Password_Error = "Please enter password between 6-16 characters"
        static let Old_Password_Error = "Please enter Old password between 6-16 characters"
        static let Confirm_Password_Error = "Please enter confirm password between 6-16 characters"
        static let Password_Match_Error = "Password doesn't match."
        static let FB_Social_Error = "Can not get your FaceBook profile"
        static let Google_Social_Error = "Can not get your Google profile"
        static let Social_Error = "Login cancelled"
    }
}
