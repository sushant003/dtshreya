//
//  Delegation.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 17/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import UIKit


protocol alertPositiveButtonDelegate {
    func alertPositiveButtonPressed()
    func alertNegativeButtonPressed()
    
}

protocol throwData {
    func sendPopedData(data : Any)
}

protocol DatePickerListener {
    func setDateFromPicker(date : Any)
    func dismissDatePicker()
}

protocol AlertTextDelegate {
    func getInputText(text:String)
}
