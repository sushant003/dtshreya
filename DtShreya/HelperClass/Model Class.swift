//
//  Model Class.swift
//  E-Commerce
//
//  Created by Sushant Jugran on 20/03/18.
//  Copyright © 2018 Sushant Jugran. All rights reserved.
//

import Foundation

struct NonActiveDashboardModel {
    var title : String = ""
    var imagelink : String = ""
    var bloglink : String = ""
    init(title:String,imagelink:String,bloglink : String) {
        self.title = title
        self.imagelink = imagelink
        self.bloglink = bloglink
    }
}

struct DashboardModel {
    var hasappointment : Int = 0
    var nextappoint : String = ""
    var address : String = "N.A"
    var hasmeal : Int = 0
    var target : String = ""
    var bells : String = ""
    var timeperiod : Int = 0
    var days : String = ""
    var achive : String = ""
    var nextMeal : [String] = [String]()
    var dietitian : [String] = [String]()
    var activities : [String:String] = [String:String]()
    var health : [String:String] = [String:String]()
}

struct DietitiansModel {
    var id : Int = 0
    var name : String = ""
    var url : String = ""
    init(name:String,url:String,id : Int) {
        self.name = name
        self.url = url
        self.id = id
    }
}

struct MessageModel {
    var time : String = ""
    var message : String = ""
    var attachedImage : String = ""
    var type : Int = 0 //1 : my text, 2 : other text, 3 : my image, 4 : other image
    init(message:String = "",time : String,image : String = "",type : Int) {
        self.message = message
        self.time = time
        self.attachedImage = image
        self.type = type
    }
}

struct MealModel {
    var mealTimeName : String
    var meal : [String]
    var collapsed : Bool
    var index : Int
    init(name:String,meal:[String],collapsed:Bool = false,mealIndex : Int) {
        mealTimeName = name
        self.meal = meal
        self.collapsed = collapsed
        index = mealIndex
    }
}

class ProgressModel: NSObject {
    var level : Int!
    var badge : Int!
    var bells : Int!
    var kgslost : Float!
}

struct DiaryModel {
    var sleeptime : String = ""
    var wakeuptime : String = ""
    var todayweight : String = ""
    var greentea : String = ""
    var waterintake : String = ""
    var pooped : String = ""
    init(sleeptime:String,wakeuptime:String,todayweight:String,greentea : String,waterintake : String,pooped : String) {
        self.sleeptime = sleeptime
        self.wakeuptime = wakeuptime
        self.todayweight = todayweight
        self.greentea = greentea
        self.waterintake = waterintake
        self.pooped = pooped
    }
}

class Food : NSObject {
    var isSelected : Bool = false
    var id : Int!
    var isExempted : Bool = false
    var name : String = ""
}
