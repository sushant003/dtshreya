//
//  ActivityCardView.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 20/02/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import UIKit

class ActivityCardView: UIView {
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor

    }
    
    func setup(activity: Activity) {
        self.awakeFromNib()
        self.addImage(withUrl: activity.imageURL)
        self.addText(text: activity.description)
    }
    
    private func addImage(withUrl imageURL: String!) {
        let imageView = UIImageView()
        imageView.downloadedFrom(link: imageURL)
        addImage(view: imageView)
    }
    
    private func addImage(withName imageName: String!) {
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image)
        addImage(view: imageView)
    }
    
    private func addImage(view imageView: UIImageView!) {
        
        imageView.frame = CGRect(x: 0, y: 0, width: self.frame.width , height: self.frame.height - 50)
        imageView.layer.masksToBounds = true
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft , .topRight], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        self.layer.mask = rectShape
        imageView.contentMode = .scaleAspectFill
        self.addSubview(imageView)
    }
    
    private func addText(text: String!) {
        let descriptionLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 50))
        descriptionLabel.center = CGPoint(x: self.frame.width/2, y: self.frame.height - 23)
        descriptionLabel.text = text
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont(name: "Avenir Next", size: 15.0)
        self.addSubview(descriptionLabel)
    }
}
