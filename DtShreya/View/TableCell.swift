//
//  TableCell.swift
//  DtShreya
//
//  Created by Sushant Jugran on 11/07/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class DietTableCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var containerView: CustomView!
    var dataArr:[String] = []
    var subMenuTable:UITableView?
    var controller : DietViewController? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpTable()
    }
    
    func setUpTable() {
        subMenuTable = UITableView(frame: CGRect.zero, style:UITableViewStyle.plain)
        subMenuTable?.isScrollEnabled = false
        subMenuTable?.separatorStyle = UITableViewCellSeparatorStyle.none
        self.addSubview(subMenuTable!)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        subMenuTable?.frame =  CGRect(x: 10, y: 40, width: Int(self.bounds.size.width - 10), height: dataArr.count*30 - 2)
        subMenuTable?.delegate = self
        subMenuTable?.dataSource = self
        containerView.frame = CGRect(x: 5, y: 5, width: Int(self.bounds.size.width - 10), height: Int(40 + dataArr.count*30 - 6))
        containerView.removeShadow()
        containerView.createShadow()
        subMenuTable?.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cellID")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cellID")
        }
        cell?.selectionStyle = .none
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 14)
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.text = dataArr[indexPath.row]
        return cell!
    }
}

class DiaryTableCell: UITableViewCell{
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var containerView: CustomView!
    @IBOutlet weak var textField: UITextField!
    
}
