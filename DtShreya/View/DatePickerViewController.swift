//
//  DatePickerViewController.swift
//  Kwikard
//
//  Created by kwikard on 27/07/17.
//  Copyright © 2017 kwikard. All rights reserved.
//

import UIKit

class DatePickerViewController: BaseViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var datePickerDelegate: DatePickerListener! = nil
    @IBOutlet weak var datePickerHeader: UILabel!
    var dateStamp: Date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (data as! [String:String])["pickerType"] == "time" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if (data as! [String:String])["date"] != nil {
                dateStamp = dateFormatter.date(from: (data as! [String:String])["date"]!)!
            }
            datePicker.setDate(dateStamp, animated: true)
            datePickerHeader.text = "Select Time"
            datePicker.datePickerMode = UIDatePickerMode.time
        }
        else {
            if (data as! [String:String])["selectPast"] == nil {
                datePicker.minimumDate =  Date()
            }
            
            datePicker.setDate(dateStamp, animated: true)
            datePickerHeader.text = "Select Date"
            datePicker.datePickerMode = UIDatePickerMode.date
        }
    }
    
    @IBAction func onClickCancelPicker(_ sender: Any) {
        datePickerDelegate.dismissDatePicker()
    }
    
    @IBAction func onClickSetPicker(_ sender: Any) {
        let dateSelected = datePicker.date
        if data != nil {
            if  (data as! [String:String])["pickerType"] == "date" {
                datePickerDelegate.setDateFromPicker(date: dateSelected)
            }
            else if (data as! [String:String])["pickerType"] == "time" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                datePickerDelegate.setDateFromPicker(date: dateFormatter.string(from: dateSelected))
            }
        }
    }
}

