//
//  Activity.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation
import UIKit

class Activity {
    private var _description : String
    private var _imageName : String
    private var _imageURL: String
    
    init(description: String, imageName: String) {
        self._description = description
        self._imageName = imageName
        self._imageURL = "https://www.gettyimages.in/landing/assets/static_content/home/info-tabs3.jpg"
    }
    
    var description: String {
        get {
            return _description
        }
    }
    var imageURL: String {
        get {
            return _imageURL
        }
    }
    var imageName: String {
        get {
            return _imageName
        }
    }
}

