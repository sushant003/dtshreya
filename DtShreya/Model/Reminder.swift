//
//  Reminder.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation

enum MealType: String {
    case breakfast = "Breakfast"
    case lunch = "Lunch"
    case postLunch = "Post Lunch"
    case eveningSnack = "Evening Snack"
    case dinner = "Dinner"
    case postDinner = "Post Dinner"
    
    func mealName() -> String {
        return self.rawValue
    }
    
}

class Reminder {
    
    var _meal: MealType
    var _reminderTime: (hours: Int, minutes: Int)?
    
    var meal: String {
        get {
            return _meal.mealName()
        }
    }
    init(meal: MealType) {
        self._meal = meal
    }
    init(meal: MealType, reminderTime: (hours: Int, minutes: Int)) {
        self._meal = meal
        
        if (reminderTime.hours > 0 && reminderTime.hours < 24 && reminderTime.minutes >= 0 && reminderTime.minutes < 60) {
            self._reminderTime = reminderTime
        }
        
    }
    
    var reminderTime: String {
        get {
            if let reminderTime = _reminderTime {
                return "\(reminderTime.hours) : \(reminderTime.minutes)"
            }
            return ""
        }
    }
}
