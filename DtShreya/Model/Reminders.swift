//
//  Reminders.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 18/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation

class Reminders {
    private var _data: [Reminder] = []
    
    init () {
        _data = [Reminder(meal: MealType.breakfast, reminderTime: (6, 00)),
                 Reminder(meal: MealType.lunch, reminderTime: (1, 10)),
                 Reminder(meal: MealType.postLunch, reminderTime: (3, 10)),
                 Reminder(meal: MealType.eveningSnack, reminderTime: (5, 10)),
                 Reminder(meal: MealType.dinner, reminderTime: (8, 10)),
                 Reminder(meal: MealType.postDinner, reminderTime: (11, 10))]
    }
    
    func addReminder(meal: MealType, time: (Int, Int)) {
        _data.append(Reminder(meal: meal, reminderTime: time))
    }
    var data: [Reminder] {
        get {
            return _data
        }
    }
}
