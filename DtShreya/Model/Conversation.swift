//
//  ChatData.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 20/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Firebase

class Conversation {
    private var _messages: [Message] = []
    private lazy var firebaseReference : DatabaseReference = DatabaseReference()
    
    var messages: [Message] {
        get {
            return _messages
        }
    }
    
    
    func sendMessage(message: String?, onCompletion: @escaping ()->Void) {
        let ref = Constants.refs.databaseChats.childByAutoId()
        if let message = message {
            if message != "" {
                let message = ["isSent": "true", "messageText": message, "date": String(describing: Date())]
                
                ref.setValue(message)
                onCompletion()
            }
        }
    }
    
    
    func observeIncomingMessages(receiverId: String,onCompletion: @escaping ()->Void) {
        firebaseReference = Database.database().reference().child("\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))-\(receiverId)")
        let query = Constants.refs.databaseChats.queryLimited(toLast: 10)
        _ = query.observe(.childAdded, with: { snapshot in
            
            if let data = snapshot.value as? [String: String] {
                self.appendMessage(data: data)
            }
            onCompletion()
        })
    }
    
    func appendMessage(data: [String: String]) {
        var isSent: Bool = false
        var messageText: String = ""
//        var date: String = ""
        
        if let _isSent = data["isSent"] {
            if _isSent == "true" {
                isSent = true
            } else {
                isSent = false
            }
        }
        if let _messageText = data["messageText"] {
            messageText = _messageText
        }
//        if let _date = data["date"] {
//            date = _date
//        }
        
        print(messageText)
        
        self._messages.append(Message(textMessage: messageText, time: Date(), isSent: isSent))
    }
    
    
}
