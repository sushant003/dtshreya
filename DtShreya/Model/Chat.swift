//
//  Chat.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation

class Chat {
    private var _chatData: [Message] = []
    
    var chatData: [Message] {
        get {
            return _chatData
        }
    }
    
    func addMessage(message: Message) {
        _chatData.append(message)
    }
    func addMessage(textMessage: String, time: Date, isSent: Bool) {
        let message: Message = Message(textMessage: textMessage, time: time, isSent: isSent)
        _chatData.append(message)
    }
}
