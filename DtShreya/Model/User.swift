//
//  User.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 09/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation

class User {
    
    static var currentUser: User = User()
    
    private var _userName: String
    private var _email: String
    private var _clientId: Int
    
    init() {
        self._userName = ""
        self._email = ""
        self._clientId = 0
    }
    
    init(clientId: Int, userName: String, email: String = "") {
        self._userName = userName
        self._email = email
        self._clientId = clientId
    }
    
    init(for data: Dictionary<String, Any>, email: String = "") {
        self._userName = String(describing: data["username"]!)
        self._email = email
        self._clientId = Int(String(describing: data["userId"]!))!
    }
    
    var id: Int {
        get {
            return _clientId
        }
    }
}
