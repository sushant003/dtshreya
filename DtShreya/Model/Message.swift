//
//  Message.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 28/03/18.
//  Copyright © 2018 Medicians. All rights reserved.
//

import Foundation

class Message {
    var textMessage: String
    var time: Date
    var isSent: Bool
    
    init() {
        textMessage = ""
        time = Date()
        isSent = false
    }
    
    init(textMessage: String, time: Date, isSent: Bool) {
        self.textMessage = textMessage
        self.time = time
        self.isSent = isSent
    }
}
