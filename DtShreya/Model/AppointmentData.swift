//
//  Appointments.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import Foundation

class AppointmentData {
    
    private var _appointmentData: [Appointment] = []
    private var _testData: [Test] = []
    
    var appointmentData: [Appointment] {
        get {
            return _appointmentData
        }
    }
    var testData: [Test] {
        get {
            return _testData
        }
    }
    
    func getAppointments(onCompletion: @escaping () -> Void) {        
        let params = ["userId": "\(AppHelper.getInstance().getUserDefaultForKey(key: AppConstants.ServiceKeys.userId))"]
        
        DataService.ds.postRequest(urlExt: "getuserappointment/", params: params) { (data, error) in
            if let data = data {
                
                if let array = data["response"] as? [[String: Any]] {
                    //If you want array of task id you can try like
                    
                    for element in array {
                        print(element)
                        self._appointmentData.append(Appointment(data: element))
                    }
                    onCompletion()
                }
                
            } else {
                
            }
        }
    }
    
    func getTests(onCompletion: @escaping () -> Void) {
        let params = ["clientId": "\(User.currentUser.id)"]
        DataService.ds.postRequest(urlExt: "getusertests/", params: params) { (data, error) in
            if let data = data {
                
                if let array = data["response"] as? [[String: Any]] {
                    //If you want array of task id you can try like
                    for element in array {
                        print(element)
                        self._testData.append(Test(data: element))
                    }
                    onCompletion()
                }
            } else {
                
            }
        }
    }
    
}
