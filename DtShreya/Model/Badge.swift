//
//  Badge.swift
//  DtShreya
//
//  Created by Shlok Kapoor on 17/04/18.
//  Copyright © 2018 Shlok. All rights reserved.
//

import UIKit

class Badge {
    var _descriprion: String
    var _image: UIImage
    var _earned: Bool
    static var numberOfBadgesEarned: Int = 0
    init(description: String, imageName: String, isEarned: Bool) {
        self._descriprion = description
        self._image = UIImage(named: imageName) ?? UIImage(named: "1")!
        self._earned = isEarned
        
        if isEarned {
            Badge.numberOfBadgesEarned += 1
        }
        
    }
    
    init(description: String, imageName: String) {
        self._descriprion = description
        self._image = UIImage(named: imageName) ?? UIImage(named: "1")!
        self._earned = false
    }
    
    var description: String {
        get {
            return _descriprion
        }
    }
    
    var image: UIImage {
        get {
            return _image
        }
    }
    
    var isEarned: Bool {
        get {
            return _earned
        }
    }
}
